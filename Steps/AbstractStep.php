<?php
namespace Brown298\ReportBuilderBundle\Steps;

use Brown298\ReportBuilderBundle\Common\Util\Inflector;
use Brown298\ReportBuilderBundle\Entity\Report;
use Brown298\ReportBuilderBundle\Event\BuildStepEvent;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\StepInterface;
use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;
use Brown298\ReportBuilderBundle\Service\PropertySelectionService;

/**
 * Class AbstractStep
 * @package Brown298\ReportBuilderBundle\Steps
 */
abstract class AbstractStep implements StepInterface
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var Report
     */
    protected $report;

    /**
     * @var PropertySelectionService
     */
    protected $selectionService;

    /**
     * @var bool
     */
    protected $requiredForView = false;

    /**
     * Constructor
     *
     * @param $key
     * @param  Report $report
     * @param  PropertySelectionService $selectionService
     * @return \Brown298\ReportBuilderBundle\Steps\AbstractStep
     */
    public function __construct($key, Report $report, PropertySelectionService $selectionService, TreeMetadata $treeMetadata)
    {
        $this->key              = $key;
        $this->report           = $report;
        $this->selectionService = $selectionService;
        $this->treeMetadata     = $treeMetadata;
    }

    /**
     * getKey
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * getLabel
     *
     * @return string
     */
    public function getLabel()
    {
        return Inflector::labelize($this->key);
    }

    /**
     * getReport
     *
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Remove restricted fields, filters, or orders
     *
     * @param  BuildStepEvent $event
     * @param  mixed          $objects
     * @return null
     */
    protected function removeRestricted(BuildStepEvent $event, $objects)
    {
        $security      = $event->getSecurity();
        $class         = $event->getClass();
        $classMetadata = null;

        if ($class) {
            $classMetadata = $this->treeMetadata->getClassMetadata($class);
        } else {
            throw new \RuntimeException("Cannot remove restricted, class is not defined in the event");
        }

        foreach ($objects as $i => $object) {

            $path = $object->getPath();
            if (!$path) {
                continue;
            }

            if (!$classMetadata->isValidPath($path)) {
                unset($objects[$i]);
            }

            $classes = $classMetadata->getClassesInPath($path);

            foreach ($classes as $class) {
                if (!$security->isViewingAllowed($class)) {
                    unset($objects[$i]);
                    continue 2;
                }
            }
        }
    }

    /**
     * isRequiredForView
     *
     * @return bool
     */
    public function isRequiredForView()
    {
        return $this->requiredForView;
    }
}