<?php
namespace Brown298\ReportBuilderBundle\Steps;

use Brown298\ReportBuilderBundle\Event\BuildStepEvent;

/**
 * Class PublishStep
 * @package Brown298\ReportBuilderBundle\Steps
 */
class PublishStep extends AbstractStep
{
    /**
     * isFinished
     *
     * @return bool
     */
    public function isFinished()
    {
        return $this->getReport()->isPublished();
    }

    /**
     * onBuildStepPreBind
     *
     * @param BuildStepEvent $event
     * @return null
     */
    public function onBuildStepPreBind(BuildStepEvent $event)
    {
        $report = $event->getReport();
        $report->setPublished(true);
    }
}