<?php
namespace Brown298\ReportBuilderBundle\Steps;

use Brown298\ReportBuilderBundle\Event\BuildStepEvent;

/**
 * Class FieldsStep
 * @package Brown298\ReportBuilderBundle\Steps
 */
class FieldsStep extends AbstractStep
{
    /**
     * @var bool
     */
    protected $requiredForView = true;

    /**
     * isFinished
     *
     * @return bool
     */
    public function isFinished()
    {
        $report = $this->getReport();

        return $report->isFieldsDecided() && $report->hasNonCountFields();
    }

    /**
     * onBuildStepPreBind
     *
     * @param BuildStepEvent $event
     * @return null
     */
    public function onBuildStepPersist(BuildStepEvent $event)
    {
        // Remove permission restricted fields
        $report = $event->getReport();
        $fields = $report->getFields();

        $this->removeRestricted($event, $fields);

        // Turn report non-aggregated if count fields have been removed
        $hasCountFields = $report->hasCountFields();

        if (!$hasCountFields) {
            $report->setAggregated(false);
        }

        // Decide fields in order to move onto the next step
        $report->setFieldsDecided(true);
    }
}
