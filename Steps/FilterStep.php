<?php
namespace Brown298\ReportBuilderBundle\Steps;

use Brown298\ReportBuilderBundle\Event\BuildStepEvent;

/**
 * Class FilterStep
 * @package Brown298\ReportBuilderBundle\Steps
 */
class FilterStep extends AbstractStep
{
    /**
     * isFinished
     *
     * @return bool
     */
    public function isFinished()
    {
        return $this->getReport()->isFilterDecided();
    }

    /**
     * onBuildStepPersist
     *
     * @param  BuildStepEvent $event
     * @return null
     */
    public function onBuildStepPersist(BuildStepEvent $event)
    {
        // Remove permission restricted filters
        $report = $event->getReport();
        $filters = $report->getFilters();

        $this->removeRestricted($event, $filters);

        // Decide filter in order to move onto the next step
        $report->setFilterDecided(true);
    }
}