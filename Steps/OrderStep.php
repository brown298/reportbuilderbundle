<?php
namespace Brown298\ReportBuilderBundle\Steps;

use Brown298\ReportBuilderBundle\Event\BuildStepEvent;

/**
 * Class OrderStep
 * @package Brown298\ReportBuilderBundle\Steps
 */
class OrderStep extends AbstractStep
{
    /**
     * isFinished
     *
     * @return bool
     */
    public function isFinished()
    {
        return $this->getReport()->getOrders()->count() > 0;
    }

    /**
     * onBuildStepPreBind
     *
     * @param BuildStepEvent $event
     * @return null
     */
    public function onBuildStepPersist(BuildStepEvent $event)
    {
        $report = $event->getReport();
        $orders = $report->getOrders();

        $this->removeRestricted($event, $orders);
    }
}