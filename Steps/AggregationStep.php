<?php
namespace Brown298\ReportBuilderBundle\Steps;

use Brown298\ReportBuilderBundle\Event\BuildStepEvent;

/**
 * Class AggregationStep
 * @package Brown298\ReportBuilderBundle\Steps
 */
class AggregationStep extends AbstractStep
{
    /**
     * @var bool
     */
    protected $requiredForView = true;

    /**
     * isFinished
     *
     * @return bool
     */
    public function isFinished()
    {
        return !is_null($this->getReport()->isAggregated());
    }

    /**
     * onBuildStepPreBind
     *
     * @param  BuildStepEvent $event
     * @return null
     */
    public function onBuildStepPersist(BuildStepEvent $event)
    {
        $report = $event->getReport();
        $aggregated = $report->isAggregated();
        $hasCountFields = $report->hasCountFields();

        if ($aggregated && !$hasCountFields) {
            $report->addCountField();
        }
    }
}
