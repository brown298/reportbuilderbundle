<?php
namespace Brown298\ReportBuilderBundle\Entity;

use Brown298\ReportBuilderBundle\Mapping\Annotation as ReportAnnotation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Report
 * @package Brown298\ReportBuilderBundle\Entity
 *
 * @ORM\Entity(repositoryClass="Brown298\ReportBuilderBundle\Entity\Repository\ReportRepository")
 * @ORM\Table(name="report")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="report_type", type="string")
 * @ORM\DiscriminatorMap({"static" = "StaticReport", "built" = "BuiltReport"})
 * @ReportAnnotation\Ignored
 */
class Report
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var Category $reportCategory
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="report_category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var date $createdOn
     *
     * @ORM\Column(name="created_on", type="datetime")
     */
    protected $createdOn;

    /**
     * @ORM\Column(name="created_by", type="string", length=255, nullable=true)
     */
    protected $createdBy;

    /**
     * @var datetime $updatedOn
     *
     * @ORM\Column(name="updated_on", type="datetime")
     */
    protected $updatedOn;

    /**
     * @ORM\Column(name="updated_by", type="string", length=255, nullable=true)
     */
    protected $updatedBy;

    /**
     * @var boolean $deleted
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    protected $deleted = false;

    /**
     * @var boolean $system
     *
     * @ORM\Column(name="system", type="boolean")
     */
    protected $system = false;

    /**
     * @var string $const
     *
     * @ORM\Column(name="const", type="string", length=255, nullable=true)
     * @ReportAnnotation\Ignored
     */
    protected $const;

    /**
     * Constructor
     *
     * @return \Brown298\ReportBuilderBundle\Entity\Report
     */
    public function __construct()
    {
        $this->setCreatedOn(new \DateTime);
        $this->setUpdatedOn(new \DateTime);

    }


    /**
     * String representation
     *
     * @return string
     */
    public function __toString()
    {
        $name = $this->getName();
        if (!empty($name)) {
            return $name;
        }

        $id = $this->getId();
        if (!empty($id)) {
            return sprintf('Report %s', $id);
        }

        $name = 'Report';
        return $name;
    }

    /**
     * getId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setName
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * setCategory
     *
     * @param ReportCategory $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * getCategory
     *
     * @return ReportCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * setCreatedOn
     *
     * @param datetime $createdOn
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * getCreatedOn
     *
     * @return datetime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * setUpdatedOn
     *
     * @param datetime $updatedOn
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;
    }

    /**
     * getUpdatedOn
     *
     * @return datetime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * setDeleted
     *
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * isDeleted
     *
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * setSystem
     *
     * @param bool $system
     */
    public function setSystem($system)
    {
        $this->system = $system;
    }

    /**
     * isSystem
     *
     * @return bool
     */
    public function isSystem()
    {
        return $this->system;
    }

    /**
     * Set const
     *
     * @param string $const
     */
    public function setConst($const)
    {
        $this->const = $const;
    }

    /**
     * Get const
     *
     * @return string
     */
    public function getConst()
    {
        return $this->const;
    }

    /**
     * isBuilt
     *
     * @return bool
     */
    public function isBuilt()
    {
        return get_class($this) === 'Brown298\ReportBuilderBundle\Entity\BuiltReport';
    }

    /**
     * isStatic
     *
     * @return bool
     */
    public function isStatic()
    {
        return get_class($this) === 'Brown298\ReportBuilderBundle\Entity\StaticReport';
    }

    /**
     * @param string $createdBy username that created the report
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        $this->setCreatedOn(new \DateTime());
        $this->setUpdatedBy($createdBy);
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
        $this->setUpdatedOn(new \DateTime());
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

}


