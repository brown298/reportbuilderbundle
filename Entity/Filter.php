<?php
namespace Brown298\ReportBuilderBundle\Entity;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportAssetInterface;
use Brown298\ReportBuilderBundle\Filter\FilterTypeFactory;
use Brown298\ReportBuilderBundle\Mapping\Annotation as ReportAnnotation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;

/**
 * Class Filter
 * @package Brown298\ReportBuilderBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="report_filter")
 * @Assert\Callback(methods={"isValid"}, groups={"filter_step"})
 * @ReportAnnotation\Ignored
 */
class Filter implements ReportAssetInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var BuiltReport $builtReport
     *
     * @ORM\ManyToOne(targetEntity="BuiltReport", inversedBy="filters")
     * @ORM\JoinColumn(name="built_report_id", referencedColumnName="id")
     */
    protected $builtReport;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=1000, nullable=true)
     */
    protected $path;

    /**
     * @var string $function
     *
     * @ORM\Column(name="func", type="string", length=150, nullable=true)
     */
    protected $function;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer")
     */
    protected $sortOrder;

    /**
     * @var string $type
     *
     * @ORM\Column(name="report_filter_type", type="string", length=100)
     */
    protected $type;

    /**
     * @var string $value
     *
     * @ORM\Column(name="value", type="string", length=100, nullable=true)
     */
    protected $value;

    /**
     * @var string $secondValue
     *
     * @ORM\Column(name="second_value", type="string", length=100, nullable=true)
     */
    protected $secondValue;

    /**
     * @var date $createdOn
     *
     * @ORM\Column(name="created_on", type="datetime")
     */
    protected $createdOn;

    /**
     * @ORM\Column(name="created_by", type="string", length=255, nullable=true)
     */
    protected $createdBy;

    /**
     * @var datetime $updatedOn
     *
     * @ORM\Column(name="updated_on", type="datetime")
     */
    protected $updatedOn;

    /**
     * @ORM\Column(name="updated_by", type="string", length=255, nullable=true)
     */
    protected $updatedBy;

    /**
     * @var
     *
     * @ORM\Column(name="runtime_filter_type", type="string", length=40, nullable=true)
     */
    protected $runtimeFilterType;

    /**
     * @var
     * @ORM\Column(name="runtime_filter_choices", type="text", nullable=true)
     */
    protected $runtimeFilterChoices;

    /**
     * Constructor
     *
     * @return \Brown298\ReportBuilderBundle\Entity\Filter
     */
    public function __construct()
    {
        $this->sortOrder = 0;

        $this->setCreatedOn(new \DateTime);
        $this->setUpdatedOn(new \DateTime);
    }

    /**
     * String representation
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->isCountFilter()) {
            return 'COUNT';
        }

        $path = $this->getPath();
        return $path;
    }

    /**
     * isValid
     *
     * @param ExecutionContext $context
     * @return bool
     */
    public function isValid(ExecutionContext $context = null)
    {
        // Path
        $path = $this->getPath();
        $countFilter = $this->isCountFilter();

        if (!$path && !$countFilter) {
            if ($context) {
                $propertyPath = sprintf('%s.path', $context->getPropertyPath());

                $message = 'Please fill in the path field';
                $context->addViolationAt($propertyPath, $message);
            }

            return false;
        }

        // Type
        $type = $this->getType();

        if (empty($type)) {
            if ($context) {
                $propertyPath = sprintf('%s.type', $context->getPropertyPath());

                $message = 'Please choose a filter type';
                $context->addViolationAt($propertyPath, $message);
            }

            return false;
        }

        // Run it past the FilterType object
        $filterType = $this->getFilterTypeObject();
        $valid = $filterType->isFilterObjectValid($this);

        if (!$valid) {

            if ($context) {
                $errors = $filterType->getValidationErrors();
                foreach ($errors as $error) {
                    $field = $error->getField();
                    $message = $error->getMessage();

                    $propertyPath = sprintf('%s.%s', $context->getPropertyPath(), $field);
                    $context->addViolationAt($propertyPath, $message);
                }
            }

            return false;
        }

        return true;
    }

    /**
     * isCountFilter
     *
     * @return bool
     */
    public function isCountFilter()
    {
        return $this->function === BuiltReport::FUNCTION_COUNT;
    }

    /**
     * getFilterTypeObject
     *
     * @return object
     */
    public function getFilterTypeObject()
    {
        $type = $this->getType();
        $filterType = FilterTypeFactory::create($type);
        return $filterType;
    }

    /**
     * getId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setBuiltReport
     *
     * @param BuiltReport $builtReport
     */
    public function setBuiltReport($builtReport)
    {
        $this->builtReport = $builtReport;
    }

    /**
     * getBuiltReport
     *
     * @return BuiltReport
     */
    public function getBuiltReport()
    {
        return $this->builtReport;
    }

    /**
     * setPath
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * getPath
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * setFunction
     *
     * @param string $function
     * @throws \InvalidArgumentException
     */
    public function setFunction($function)
    {
        $allowedFunctions = array(BuiltReport::FUNCTION_COUNT);
        if (!is_null($function) && !in_array($function, $allowedFunctions)) {
            throw new \InvalidArgumentException('Invalid function');
        }

        $this->function = $function;
    }

    /**
     * getFunction
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * setSortOrder
     *
     * @param integer $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * getSortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * setType
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * getType
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * setValue
     *
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * getValue
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * setSecondValue
     *
     * @param string $secondValue
     */
    public function setSecondValue($secondValue)
    {
        $this->secondValue = $secondValue;
    }

    /**
     * getSecondValue
     *
     * @return integer
     */
    public function getSecondValue()
    {
        return $this->secondValue;
    }

    /**
     * setCreatedOn
     *
     * @param \DateTime $createdOn
     */
    public function setCreatedOn(\DateTime $createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * getCreatedOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * setUpdatedOn
     *
     * @param \DateTime $updatedOn
     */
    public function setUpdatedOn(\DateTime $updatedOn)
    {
        $this->updatedOn = $updatedOn;
    }

    /**
     * getUpdatedOn
     *
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        $this->setCreatedOn(new \DateTime());
        $this->setUpdatedBy($createdBy);
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
        $this->setUpdatedOn(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @return int
     */
    public function isParameter()
    {
        return ( preg_match('/^:/', $this->getValue()) );
    }

    /**
     * @return mixed
     */
    public function getRuntimeFilterType()
    {
        return $this->runtimeFilterType;
    }

    /**
     * @param mixed $runtimeFilterType
     */
    public function setRuntimeFilterType($runtimeFilterType)
    {
        $this->runtimeFilterType = $runtimeFilterType;
    }

    /**
     * @return mixed
     */
    public function getRuntimeFilterChoices()
    {
        return $this->runtimeFilterChoices;
    }

    /**
     * @param mixed $runtimeFilterChoices
     */
    public function setRuntimeFilterChoices($runtimeFilterChoices)
    {
        $this->runtimeFilterChoices = $runtimeFilterChoices;
    }
}
