<?php
namespace Brown298\ReportBuilderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Brown298\ReportBuilderBundle\Mapping\Annotation as ReportAnnotation;

/**
 * Class StaticReport
 * @package Brown298\ReportBuilderBundle\Entity
 * @ORM\Entity
 * @ReportAnnotation\Ignored
 */
class StaticReport extends Report
{

    /**
     * @var string $service
     *
     * @ORM\Column(name="service", type="string", length=150)
     */
    protected $service;

    /**
     * setService
     *
     * @param string $service
     * @return null
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * getService
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }
}