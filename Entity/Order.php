<?php
namespace Brown298\ReportBuilderBundle\Entity;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportAssetInterface;
use Brown298\ReportBuilderBundle\Mapping\Annotation as ReportAnnotation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;


/**
 * Class Order
 * @package Brown298\ReportBuilderBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="report_order")
 * @Assert\Callback(methods={"isValid"}, groups={"order_step"})
 * @ReportAnnotation\Ignored
 */
class Order implements ReportAssetInterface
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var BuiltReport $builtReport
     *
     * @ORM\ManyToOne(targetEntity="BuiltReport", inversedBy="orders")
     * @ORM\JoinColumn(name="built_report_id", referencedColumnName="id")
     */
    protected $builtReport;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=1000, nullable=true)
     */
    protected $path;

    /**
     * @var string $function
     *
     * @ORM\Column(name="func", type="string", length=150, nullable=true)
     */
    protected $function;

    /**
     * @var boolean $ascending
     *
     * @ORM\Column(name="ascending", type="boolean")
     * @Assert\NotBlank(groups={"order_step"})
     */
    protected $ascending;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer")
     */
    protected $sortOrder;

    /**
     * @var \DateTime $createdOn
     *
     * @ORM\Column(name="created_on", type="datetime")
     */
    protected $createdOn;

    /**
     * @ORM\Column(name="created_by", type="string", length=255, nullable=true)
     */
    protected $createdBy;

    /**
     * @var \DateTime $updatedOn
     *
     * @ORM\Column(name="updated_on", type="datetime")
     */
    protected $updatedOn;

    /**
     * @ORM\Column(name="updated_by", type="string", length=255, nullable=true)
     */
    protected $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ascending = true;
        $this->sortOrder = 0;

        $this->setCreatedOn(new \DateTime);
        $this->setUpdatedOn(new \DateTime);
    }

    /**
     * String representation
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->isCountOrder()) {
            $base = 'Count';
        } else {
            $base = $this->getPath();
        }

        $ascending = $this->isAscending() ? 'ascending' : 'descending';

        $output = sprintf('%s, %s', $base, $ascending);

        return $output;
    }

    /**
     * isValid
     *
     * @param ExecutionContext $context
     * @return null
     */
    public function isValid(ExecutionContext $context)
    {
        // Path
        $path = $this->getPath();
        $countOrder = $this->isCountOrder();

        if (!$path && !$countOrder) {
            $propertyPath = sprintf('%s.path', $context->getPropertyPath());

            $message = 'Please fill in the path field';
            $context->addViolationAt($propertyPath, $message);
        }
    }

    /**
     * isCountOrder
     *
     * @return bool
     */
    public function isCountOrder()
    {
        return $this->function === BuiltReport::FUNCTION_COUNT;
    }

    /**
     * getId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setBuiltReport
     *
     * @param BuiltReport $builtReport
     */
    public function setBuiltReport($builtReport)
    {
        $this->builtReport = $builtReport;
    }

    /**
     * getBuiltReport
     *
     * @return BuiltReport
     */
    public function getBuiltReport()
    {
        return $this->builtReport;
    }

    /**
     * setPath
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * getPath
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * setFunction
     *
     * @param string $function
     * @throws \InvalidArgumentException
     */
    public function setFunction($function)
    {
        $allowedFunctions = array(BuiltReport::FUNCTION_COUNT);
        if (!is_null($function) && !in_array($function, $allowedFunctions)) {
            throw new \InvalidArgumentException('Invalid function');
        }

        $this->function = $function;
    }

    /**
     * getFunction
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * setAscending
     *
     * @param bool $ascending
     */
    public function setAscending($ascending)
    {
        $this->ascending = $ascending;
    }

    /**
     * getAscending
     *
     * @return bool
     */
    public function isAscending()
    {
        return $this->ascending;
    }

    /**
     * setSortOrder
     *
     * @param integer $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * getSortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * setCreatedOn
     *
     * @param \DateTime $createdOn
     */
    public function setCreatedOn(\DateTime $createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * getCreatedOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * setUpdatedOn
     *
     * @param \DateTime $updatedOn
     */
    public function setUpdatedOn(\DateTime $updatedOn)
    {
        $this->updatedOn = $updatedOn;
    }

    /**
     * getUpdatedOn
     *
     * @return datetime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        $this->setCreatedOn(new \DateTime);
        $this->setUpdatedBy($createdBy);
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
        $this->setUpdatedOn(new \DateTime);
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}

