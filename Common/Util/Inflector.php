<?php
namespace Brown298\ReportBuilderBundle\Common\Util;

use \Doctrine\Common\Util\Inflector as BaseInflector;

/**
 * Class Inflector
 * @package Brown298\ReportBuilderBundle\Common\Util
 */
class Inflector extends BaseInflector
{
    /**
     * Words that should be converted to uppercase
     */
    public static $uppercaseWords = array('id');

    /**
     * Converts 'ModelName' to 'Model Name'
     * Converts 'Namespace\ModelName' to 'Model Name'
     * Converts 'MyBundle:ModelName' to 'Model Name'
     * Converts 'camelCased' to 'Camel Cased'
     * Converts 'underscored_words' to 'Underscored Words'
     * Converts 'property_id' to 'Property'
     * Converts 'property_uuid' to 'Property UUID'
     *
     * @param mixed $word
     *
     * @return string
     */
    public static function labelize($word)
    {
        $words = self::getWords($word);
        $label = implode(' ', $words);
        $label = ucwords($label);
        $label = preg_replace('/ id$/i', '', $label);

        $upperWords = array_map('preg_quote', self::$uppercaseWords);
        $upperWords = implode('|', self::$uppercaseWords);
        $search = sprintf('/\b(%s)\b/i', $upperWords);
        $label = preg_replace_callback($search, 'self::labelizeCallback', $label);

        return $label;
    }

    /**
     * labelizeCallback
     *
     * @param array $matches
     *
     * @return string
     */
    public static  function labelizeCallback(array $matches)
    {
        return strtoupper($matches[0]);
    }

    /**
     * getWords
     *
     * @param string $word
     *
     * @return array
     */
    public static function getWords($word)
    {
        $class = static::getClass($word);

        $class = explode(':', $class);
        $class = array_pop($class);

        $string = $class;
        $string = preg_replace('/(?<=\w)([A-Z])/', ' $1', $string);
        $string = str_replace(array('-', '_', '/', '\\'), ' ', $string);

        $words = explode(' ', $string);
        $words = array_map('trim', $words);
        $words = array_filter($words);

        return $words;
    }

    /**
     * getNameFromClass
     *
     * @param  string|object $class
     * @return string
     */
    public static function getNameFromClass($class)
    {
        if (is_object($class)) {
            $class = get_class($class);
        }

        $name = $class;
        $name = str_replace('\\', ' ', $name);
        $name = strtolower($name);
        $name = implode('_', array_filter(explode(' ', $name)));

        return $name;
    }

    /**
     * Get class from full namespace example Test\Bundle\ClassName returns
     * ClassName
     *
     * @param string $namespace
     *
     * @return string
     */
    public static function getClass($namespace)
    {
        $class = explode('\\', $namespace);

        return end($class);
    }

    /**
     * getHumanBytes
     *
     * @param integer $bytes bytes
     * @param float|int $decimals decimals
     *
     * @return string
     * @static
     */
    public static function getHumanBytes($bytes, $decimals = 0)
    {
        $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        $exponent = floor((strlen($bytes) - 1) / 3);
        $modifiedBytes = $bytes / pow(1024, $exponent);
        $measurement = @$sizes[$exponent];

        $format = '%.' . $decimals . 'f %s';
        $human = sprintf($format, $modifiedBytes, $measurement);

        return $human;
    }

    /**
     * Convert a string into a constant string for the const field
     *
     * @param string $name string name to convert
     * @param bool   $trim boolean whether to trim or not
     *
     * @return string
     */
    public static function convertConst($name, $trim = true)
    {
        if ($trim) {
            $name = substr($name, 0, 40);
        }

        $name = strtoupper($name);
        $name = str_replace(' ', '_', $name);

        return $name;
    }

    /**
     * Convert a string into a constant string
     *
     * @param  string          $name
     * @param  integer|boolean $truncate Number of characters to truncate to, or false to not truncate
     * @return string
     */
    public static function convertToConst($name, $truncate = 40)
    {
        if ($truncate !== false) {
            $name = substr($name, 0, $truncate);
        }

        $name = implode('_', self::getWords($name));
        $name = strtoupper($name);

        return $name;
    }
}