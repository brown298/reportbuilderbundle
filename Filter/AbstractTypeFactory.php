<?php
namespace Brown298\ReportBuilderBundle\Filter;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\DBAL\Exception\InvalidArgumentException;

/**
 * Class AbstractTypeFactory
 * @package Brown298\ReportBuilderBundle\Filter
 */
abstract class AbstractTypeFactory
{
    public static $classNamePattern = '%s';

    /**
     * Create
     *
     * @param string $key
     * @return null
     * @throws InvalidArgumentException
     */
    public static function create($key)
    {
        if (is_array($key)) {
            return self::createMultiple($key);
        }

        $name = Inflector::classify($key);
        $class = sprintf(static::$classNamePattern, $name);

        if (!class_exists($class)) {
            $message = sprintf('Class %s does not exist', $class);
            throw new InvalidArgumentException($message);
        }

        $filterType = new $class($key);

        return $filterType;
    }

    /**
     * createMultiple
     *
     * @param $keys
     * @return array
     * @throws InvalidArgumentException
     */
    private static function createMultiple($keys)
    {
        if (!$keys) {
            return array();
        }

        $types = array();

        foreach ($keys as $key) {
            if (!is_string($key)) {
                $message = 'Invalid key';
                throw new InvalidArgumentException($message);
            }

            $type = self::create($key);

            $types[] = $type;
        }

        return $types;
    }
}