<?php
namespace Brown298\ReportBuilderBundle\Filter;


/**
 * FilterTypeMap
 *
 */
class FilterTypeMap
{
    /**
     * @var array
     */
    protected static $keysForCount = array(
        'equal_to_number',
        'not_equal_to_number',
        'greater_than',
        'greater_than_or_equal_to',
        'less_than',
        'less_than_or_equal_to',
        'between_numbers',
        'not_between_numbers',
    );

    /**
     * getKeysForColumn
     *
     * @param string $columnType
     *
     * @return array
     */
    public static function getKeysForColumn($columnType, $valueType = 'types')
    {
        $map = self::getSimpleMap();

        foreach ($map as $entry) {
            if (in_array($columnType, $entry['columns'])) {
                return $entry[$valueType];
            }
        }

        return null;
    }

    /**
     * getSimpleMap
     *
     * @return array
     */
    private static function getSimpleMap()
    {
        $map = array();

        $mapEntry = array();
        $mapEntry['columns'] = array('string', 'text');
        $mapEntry['types'] = array(
            'contains',
            'does_not_contain',
            'equal_to',
            'not_equal_to',
            'starts_with',
            'ends_with',
            'empty_text',
            'not_empty_text',
        );
        $mapEntry['runtime'] = array('Text', 'Choice');
        $map[] = $mapEntry;

        $mapEntry = array();
        $mapEntry['columns'] = array('boolean');
        $mapEntry['types'] = array(
            'true',
            'false',
        );
        $mapEntry['runtime'] = array();
        $map[] = $mapEntry;

        $mapEntry = array();
        $mapEntry['columns'] = array('integer', 'decimal', 'bigint', 'smallint', 'float');
        $mapEntry['types'] = array(
            'equal_to_number',
            'not_equal_to_number',
            'greater_than',
            'greater_than_or_equal_to',
            'less_than',
            'less_than_or_equal_to',
            'between_numbers',
            'not_between_numbers',
            'empty_number',
            'not_empty_number',
        );
        $mapEntry['runtime'] = array('Integer', 'Decimal');
        $map[] = $mapEntry;

        $mapEntry = array();
        $mapEntry['columns'] = array('date');
        $mapEntry['types'] = array(
            'on_date',
            'on_or_after_date',
            'on_or_before_date',
            'between_dates',
            'not_between_dates',
            'empty',
            'not_empty',
        );
        $mapEntry['runtime'] = array('Date');
        $map[] = $mapEntry;

        $mapEntry = array();
        $mapEntry['columns'] = array('datetime');
        $mapEntry['types'] = array(
            'on_datetime',
            'on_or_after_datetime',
            'on_or_before_datetime',
            'between_datetimes',
            'not_between_datetimes',
            'empty',
            'not_empty',
        );
        $mapEntry['runtime'] = array('Date', 'DateTime');
        $map[] = $mapEntry;

        return $map;
    }

    /**
     * getKeysForCount
     *
     * @return array
     */
    public static function getKeysForCount()
    {
        return self::$keysForCount;
    }
}