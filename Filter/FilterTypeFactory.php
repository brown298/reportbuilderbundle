<?php
namespace Brown298\ReportBuilderBundle\Filter;

use Doctrine\Common\Util\Inflector;
use InvalidArgumentException;

/**
 * Class FilterTypeFactory
 * @package Brown298\ReportBuilderBundle\Filter
 */
class FilterTypeFactory extends AbstractTypeFactory
{
    /**
     * Pattern to find names of filter type classes
     */
    public static $classNamePattern = 'Brown298\ReportBuilderBundle\Filter\Type\%sFilterType';
}