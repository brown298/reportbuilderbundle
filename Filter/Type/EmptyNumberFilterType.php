<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * EmptyNumberFilterType
 *
 */
class EmptyNumberFilterType extends EmptyFilterType implements FilterTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false)
    {
        $paramKey  = $this->createQbParameterKey($qb);
        $expr      = $qb->expr();
        $subExpr   = $expr->eq($property, '?'.$paramKey);
        $finalExpr = $expr->orX(sprintf('%s IS NULL', $property), $subExpr);

        if ($having) {
            $qb->andHaving($finalExpr);
        } else {
            $qb->andWhere($finalExpr);
        }

        $qb->setParameter($paramKey, 0);
        return $qb;
    }
}