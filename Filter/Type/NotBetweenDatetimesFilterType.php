<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Class NotBetweenDatetimesFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
class NotBetweenDatetimesFilterType extends NotBetweenDatesFilterType implements FilterTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false)
    {
        $paramKey1 = $this->createQbParameterKey($qb);
        $paramKey2 = $this->createQbParameterKey($qb);

        $value = $filter->getValue();
        $secondValue = $filter->getSecondValue();

        $expr = $qb->expr()->between($property, '?'.$paramKey1, '?'.$paramKey2);
        $expr = $qb->expr()->not($expr);

        if ($having) {
            $qb->andHaving($expr);
        } else {
            $qb->andWhere($expr);
        }

        $qb->setParameter($paramKey1, new \DateTime($value.' 00:00:01'));
        $qb->setParameter($paramKey2, new \DateTime($secondValue.' 23:59:59'));

        return $qb;
    }
}