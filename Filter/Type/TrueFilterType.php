<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Class TrueFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
class TrueFilterType extends AbstractFilterType implements FilterTypeInterface
{
    /**
     * @var string
     */
    protected $optionLabel = 'is true';

    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false)
    {
        $expression = sprintf('%s <> false', $property);

        if ($having) {
            $qb->andHaving($expression);
        } else {
            $qb->andWhere($expression);
        }

        return $qb;
    }
}