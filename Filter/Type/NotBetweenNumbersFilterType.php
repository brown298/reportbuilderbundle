<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Class NotBetweenNumbersFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
class NotBetweenNumbersFilterType extends AbstractFilterType implements FilterTypeInterface
{
    /**
     * @var string
     */
    protected $optionLabel = 'is not between';

    /**
     * @var string
     */
    protected $formTemplate = 'Brown298ReportBuilderBundle:Build:Filter/two-text-inputs.html.twig';

    /**
     * isFilterObjectValid
     *
     * @param Filter $filter
     * @return bool
     */
    public function isFilterObjectValid(Filter $filter)
    {
        $value = $filter->getValue();
        $secondValue = $filter->getSecondValue();

        if (!is_string($value) || !is_string($secondValue)) {
            $this->addValidationError('value', 'Invalid argument');
            return false;
        }

        if (!is_numeric($value) || !is_numeric($secondValue)) {
            $this->addValidationError('value', 'Please enter valid numbers');
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false)
    {
        return $this->applyComparisonFilter($qb, $filter, $property, 'between', true, $having);
    }
}