<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Class EmptyTextFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
class EmptyTextFilterType extends EmptyFilterType implements FilterTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false )
    {
        $paramKey = $this->createQbParameterKey($qb);

        $expr = $qb->expr()->orX(
            sprintf('%s IS NULL', $property),
            $qb->expr()->eq($property, '?' . $paramKey)
        );

        if ($having) {
            $qb->andHaving($expr);
        } else {
            $qb->andWhere($expr);
        }

        $qb->setParameter($paramKey, '');

        return $qb;
    }
}