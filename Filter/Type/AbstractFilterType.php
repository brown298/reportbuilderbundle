<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Common\Util\Inflector;
use Brown298\ReportBuilderBundle\Entity\Filter;
use Brown298\ReportBuilderBundle\Filter\ValidationError;

/**
 * Class AbstractFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
abstract class AbstractFilterType
{
    /**
     * @var string
     */
    protected $optionLabel;

    /**
     * @var string
     */
    protected $formTemplate = 'Brown298ReportBuilderBundle:Build:Filter/empty.html.twig';

    /**
     * @var string
     */
    private $key;

    /**
     * @var array
     */
    private $validationErrors = array();

    /**
     * Constructor
     *
     * @param mixed $key
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * getKey
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * getLabel
     *
     * @return string
     */
    public function getLabel()
    {
        $key = $this->getKey();
        $label = Inflector::labelize($key);
        return $label;
    }

    /**
     * getOptionLabel
     *
     * @return string
     */
    public function getOptionLabel()
    {
        if ($this->optionLabel) {
            return $this->optionLabel;
        }

        $label = $this->getLabel();
        $label = strtolower($label);
        return $label;
    }

    /**
     * getFormTemplate
     *
     * @return string
     */
    public function getFormTemplate()
    {
        return $this->formTemplate;
    }

    /**
     * addValidationError
     *
     * @param string $field
     * @param string $message
     * @return null
     */
    public function addValidationError($field, $message)
    {
        $validationError = new ValidationError($field, $message);
        $this->validationErrors[] = $validationError;
    }

    /**
     * getValidationErrors
     *
     * @return array
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * isFilterObjectValid
     *
     * @param Filter $filter
     * @return bool
     */
    public function isFilterObjectValid(Filter $filter)
    {
        return true;
    }

    /**
     * isDateValid
     *
     * @param string $date
     * @return bool
     */
    public function isDateValid($date)
    {
        if (!is_string($date)) {
            return false;
        }

        if (
            !preg_match('/\d{2}\/\d{2}\/\d{4}/', $date)
            && !preg_match('/\d{4}-\d{2}-\d{2}/', $date)
            && !preg_match('/[0-9]+ day/', $date)
        ) {
            return false;
        }

        if (!strtotime($date)) {
            return false;
        }

        return true;
    }

    /**
     * Returns the first unused parameter index. Always returns a unique index,
     * even setParameter has not yet been called on the query builder object
     *
     * @param QueryBuilder $qb
     * @return integer
     */
    public function createQbParameterKey(QueryBuilder $qb)
    {
        static $registry = array();
        $hash = spl_object_hash($qb);

        $i = 1;
        while (true) {
            $param = $qb->getParameter($i);

            if (is_null($param) && !isset($registry[$hash][$i])) {

                // Register creation
                if (!isset($registry[$hash])) {
                    $registry[$hash] = array();
                }
                $registry[$hash][$i] = true;

                break;
            }

            $i++;
        }

        return $i;
    }

    /**
     * Applies a comparison filter to QueryBuilder object
     *
     * @param  QueryBuilder $qb
     * @param \Brown298\ReportBuilderBundle\Entity\Filter $filter
     * @param  string $property "e.prop"
     * @param $method
     * @param bool $inverse
     * @param  bool $having
     * @return null
     */
    protected function applyComparisonFilter(QueryBuilder $qb, Filter $filter, $property, $method, $inverse = false, $having = false)
    {
        $paramKey = $this->createQbParameterKey($qb);
        $value = $filter->getValue();

        $useSecondParameter = ($method === 'between');

        if ($useSecondParameter) {
            $paramKey2 = $this->createQbParameterKey($qb);
            $value2 = $filter->getSecondValue();
            $qb->setParameter($paramKey2, $value2);
            $expr = $qb->expr()->$method($property, '?' . $paramKey, '?'.$paramKey2);
        } else {
            $expr = $qb->expr()->$method($property, '?' . $paramKey);
        }

        if ($inverse) {
            $expr = $qb->expr()->not($expr);
        }

        if ($having) {
            $qb->andHaving($expr);
        } else {
            $qb->andWhere($expr);
        }

        $qb->setParameter($paramKey, $value);

        return $qb;
    }
}