<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * BetweenDatesFilterType
 *
 */
class BetweenDatesFilterType extends AbstractFilterType implements FilterTypeInterface
{
    /**
     * @var string
     */
    protected $optionLabel = 'is between';

    /**
     * @var string
     */
    protected $formTemplate = 'Brown298ReportBuilderBundle:Build:Filter/two-text-inputs.html.twig';

    /**
     * isFilterObjectValid
     *
     * @param Filter $filter
     * @return bool
     */
    public function isFilterObjectValid(Filter $filter)
    {
        $value = $filter->getValue();
        $secondValue = $filter->getSecondValue();

        if (!is_string($value) || !is_string($secondValue)) {
            $this->addValidationError('value', 'Invalid argument');
            return false;
        }

        if (!$this->isDateValid($value) || !$this->isDateValid($secondValue)) {
            $this->addValidationError('value', 'Please enter valid dates');
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false) {
        $paramKey1   = $this->createQbParameterKey($qb);
        $paramKey2   = $this->createQbParameterKey($qb);
        $value       = $filter->getValue();
        $secondValue = $filter->getSecondValue();

        $expr = $qb->expr()->between($property, '?'.$paramKey1, '?'.$paramKey2);

        if ($having) {
            $qb->andHaving($expr);
        } else {
            $qb->andWhere($expr);
        }

        $qb->setParameter($paramKey1, new \DateTime($value));
        $qb->setParameter($paramKey2, new \DateTime($secondValue));

        return $qb;
    }
}