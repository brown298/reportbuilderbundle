<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Class NotEmptyTextFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
class NotEmptyTextFilterType extends NotEmptyFilterType implements FilterTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false)
    {
        $paramKey = $this->createQbParameterKey($qb);

        $expr = $qb->expr()->orx(
            sprintf('%s IS NULL', $property),
            $qb->expr()->eq($property, '?'.$paramKey)
        );
        $expr = $qb->expr()->not($expr);

        if ($having) {
            $qb->andHaving($expr);
        } else {
            $qb->andWhere($expr);
        }

        $qb->setParameter($paramKey, '');

        return $qb;
    }
}