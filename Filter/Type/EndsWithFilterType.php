<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Class EndsWithFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
class EndsWithFilterType extends AbstractFilterType implements FilterTypeInterface
{
    /**
     * @var string
     */
    protected $formTemplate = 'Brown298ReportBuilderBundle:Build:Filter/one-text-input.html.twig';

    /**
     * isFilterObjectValid
     *
     * @param Filter $filter
     * @return bool
     */
    public function isFilterObjectValid(Filter $filter)
    {
        $value = $filter->getValue();

        if (!is_string($value)) {
            $this->addValidationError('value', 'Invalid argument');
            return false;
        }

        if (empty($value)) {
            $this->addValidationError('value', 'Please enter a value');
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false) {
        $value = $filter->getValue();

        $comparison = $qb->expr()->literal('%' . $value);
        $expr = $qb->expr()->like($property, $comparison);

        if ($having) {
            $qb->andHaving($expr);
        } else {
            $qb->andWhere($expr);
        }

        return $qb;
    }
}
