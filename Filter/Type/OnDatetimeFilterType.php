<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Class OnDatetimeFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
class OnDatetimeFilterType extends OnDateFilterType implements FilterTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false)
    {
        $paramKey = $this->createQbParameterKey($qb);
        $value = $filter->getValue();

        $expr = $qb->expr()->eq($property, '?'.$paramKey);

        if ($having) {
            $qb->andHaving($expr);
        } else {
            $qb->andWhere($expr);
        }

        $date = new \DateTime($value);
        $qb->setParameter($paramKey, $date->format('Y-m-d H:i:s'));

        return $qb;
    }
}