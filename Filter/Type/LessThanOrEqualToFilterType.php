<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Class LessThanOrEqualToFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
class LessThanOrEqualToFilterType extends AbstractFilterType implements FilterTypeInterface
{
    /**
     * @var string
     */
    protected $optionLabel = 'is less than or equal to';

    /**
     * @var string
     */
    protected $formTemplate = 'Brown298ReportBuilderBundle:Build:Filter/one-text-input.html.twig';

    /**
     * isFilterObjectValid
     *
     * @param Filter $filter
     * @return bool
     */
    public function isFilterObjectValid(Filter $filter)
    {
        $value = $filter->getValue();

        if (!is_string($value)) {
            $this->addValidationError('value', 'Invalid argument');
            return false;
        }

        if (!is_numeric($value) && !preg_match('/^:.*/', $value)) {
            $this->addValidationError('value', 'Please enter a valid number');
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false) {
        return $this->applyComparisonFilter($qb, $filter, $property, 'lte', false, $having);
    }
}
