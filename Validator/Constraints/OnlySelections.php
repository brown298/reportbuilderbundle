<?php
namespace Brown298\ReportBuilderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class OnlySelections
 * @package Brown298\ReportBuilderBundle\Validator\Constraints
 * @Annotation
 */
class OnlySelections extends Constraint
{
    /**
     * @var string
     */
    public $message = 'The following invalid fields have been removed from the list: %disallowedList%';

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'only_selection';
    }

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }

}