<?php
namespace EventListener;

use Brown298\ReportBuilderBundle\EventListener\BuildStepListener;
use Brown298\ReportBuilderBundle\Event\BuildStepEvent;
use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class BuildStepListenerTest
 * @package EventListener
 */
class BuildStepListenerTest extends AbstractTest
{
    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Entity\BuiltReport
     */
    protected $report;

    /**
     * @Mock
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Service\Security\NullSecurityService
     */
    protected $security;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface
     */
    protected $classMetadata;

    /**
     * @var string
     */
    protected $stepClassTemplate = 'Brown298\ReportBuilderBundle\Steps\%sStep';

    /**
     * testOnBuildStepPreBind
     */
    public function testOnBuildStepPreBind()
    {
        $step = $this->buildStep('Publish');
        $event = $this->buildEvent($step);

        $entity = new BuildStepListener;
        $entity->onBuildStepPreBind($event);

        Phake::verify($step)->onBuildStepPreBind($event);
    }

    /**
     * testOnBuildStepPreBindNotCallable
     */
    public function testOnBuildStepPreBindNotCallable()
    {
        $step = $this->buildStep('Fields');
        $event = $this->buildEvent($step);

        $entity = new BuildStepListener;
        $entity->onBuildStepPreBind($event);

        Phake::verify($step, Phake::never())->onBuildStepPreBind();
    }

    /**
     * testOnBuildStepPersist
     */
    public function testOnBuildStepPersist()
    {
        $step = $this->buildStep('Order');
        $event = $this->buildEvent($step);

        $entity = new BuildStepListener;
        $entity->onBuildStepPersist($event);

        Phake::verify($step)->onBuildStepPersist($event);
    }

    /**
     * testOnBuildStepPersistNotCallable
     */
    public function testOnBuildStepPersistNotCallable()
    {
        $step = $this->buildStep('Publish');
        $event = $this->buildEvent($step);

        $entity = new BuildStepListener;
        $entity->onBuildStepPersist($event);

        Phake::verify($step, Phake::never())->onBuildStepPersist();
    }

    /**
     * buildStep
     *
     * @param $name
     * @return mixed
     */
    private function buildStep($name)
    {
        $class = sprintf($this->stepClassTemplate, $name);
        $step  = Phake::mock($class);

        return $step;
    }

    /**
     * buildEvent
     *
     * @param $step
     * @return BuildStepEvent
     */
    private function buildEvent($step)
    {
        return Phake::partialMock('\Brown298\ReportBuilderBundle\Event\BuildStepEvent', $this->report, $step, $this->em, $this->security, $this->classMetadata);
    }
} 