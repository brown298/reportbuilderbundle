<?php
namespace Filter\Type;

use Phake;

/**
 * Class OnOrBeforeDateFilterTypeTest
 *
 * @package Filter\Type
 */
class OnOrAfterDateFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\OnOrAfterDateFilterType';

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->gte(Phake::anyParameters())->thenReturn('comparison');
    }

    /**
     * applyFilterProvider
     *
     * @return mixed
     */
    function applyFilterProvider()
    {
        return array(
            array(true,  'comparison'),
            array(false, 'comparison'),
        );
    }

    /**
     * testApplyFilter
     * @dataProvider applyFilterProvider
     */
    public function testApplyFilter($having, $message)
    {
        parent::testApplyFilter($having, $message);
        Phake::verify($this->qb)->setParameter($this->anything(), Phake::capture($date));
        $this->assertInstanceOf('\DateTime', $date);
    }

    /**
     * isFilterObjectValidProvider
     *
     */
    public function isFilterObjectValidProvider()
    {
        return array(
            array('test', false),
            array('', false),
            array(null, false),
            array('01/01/2010', true),
        );
    }

    /**
     * testIsFilterObjectValid
     *
     * @dataProvider isFilterObjectValidProvider
     */
    public function testIsFilterObjectValid($value, $exptectedResult)
    {
        Phake::when($this->filter)->getValue()->thenReturn($value);
        $result = $this->filterType->isFilterObjectValid($this->filter);

        if ($exptectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is on or after', $this->filterType->getOptionLabel());
    }
} 