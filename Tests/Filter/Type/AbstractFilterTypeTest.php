<?php
namespace Filter\Type;

use Phake;
use Brown298\TestExtension\Test\AbstractTest;

/**
 * Class AbstractFilterTypeTest
 *
 * @package Filter\Type
 */
abstract class AbstractFilterTypeTest extends AbstractTest
{
    /**
     * @var string
     */
    protected $filterTypeName = '';

    /**
     * @var string
     */
    protected $key = 'key';

    /**
     * @Mock
     * @var \Doctrine\ORM\QueryBuilder
     */
    protected $qb;

    /**
     * @Mock
     * @var \Doctrine\ORM\Query\Expr
     */
    protected $expr;

    /**
     * @Mock
     * @var \Doctrine\ORM\Query\Expr\Literal
     */
    protected $literal;
    /**
     * @Mock
     * @var \Doctrine\ORM\Query\Expr\Comparison
     */
    protected $comparison;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Entity\Filter
     */
    protected $filter;

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface
     */
    protected $filterType;

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->qb)->expr()->thenReturn($this->expr);
        Phake::when($this->expr)->literal(Phake::anyParameters())->thenReturn($this->literal);
        Phake::when($this->expr)->like(Phake::anyParameters())->thenReturn($this->comparison);
        Phake::when($this->expr)->lte(Phake::anyParameters())->thenReturn($this->comparison);
        Phake::when($this->expr)->gte(Phake::anyParameters())->thenReturn($this->comparison);
        Phake::when($this->expr)->eq(Phake::anyParameters())->thenReturn($this->comparison);
        Phake::when($this->expr)->orX(Phake::anyParameters())->thenReturn($this->comparison);
        Phake::when($this->expr)->between(Phake::anyParameters())->thenReturn($this->comparison);
        $this->filterType = new $this->filterTypeName($this->key);
    }

    /**
     * testApplyFilterReturnsQb
     *
     * ensures filter types return a querybuilder when running apply filter
     */
    public function testApplyFilterReturnsQb()
    {
        $result = $this->filterType->applyFilter($this->qb, $this->filter, 'test', false);
        $this->assertInstanceOf('Doctrine\ORM\QueryBuilder', $result);
    }

    /**
     * applyFilterProvider
     *
     * @return mixed
     */
    abstract function applyFilterProvider();

    /**
     * testApplyFilter
     * @dataProvider applyFilterProvider
     */
    public function testApplyFilter($having, $message)
    {
        $result = $this->filterType->applyFilter($this->qb, $this->filter, 'testProperty', $having);
        if ($having) {
            Phake::verify($result)->andHaving($message);
        } else {
            Phake::verify($result)->andWhere($message);
        }
    }

    /**
     * testGetKey
     *
     */
    public function testGetKey()
    {
        $key = 'test';
        $obj = new $this->filterTypeName($key);
        $this->assertEquals($key, $obj->getKey());
    }

    /**
     * getLabelProvider
     *
     */
    public function getLabelProvider()
    {
        return array(
            array('test', 'Test'),
            array('test_value', 'Test Value'),
        );
    }

    /**
     * testGetLabel
     *
     * @dataProvider getLabelProvider
     *
     * @param $key
     * @param $label
     */
    public function testGetLabel($key, $label)
    {
        $obj = new $this->filterTypeName($key);
        $this->assertEquals($label, $obj->getLabel());
    }

    /**
     * testGetFormTemplate
     *
     */
    public function testGetFormTemplate()
    {
        $this->assertRegExp('/.+twig$/', $this->filterType->getFormTemplate());
    }

    /**
     * testGetOptionLabelNotEmpty
     *
     */
    public function testGetOptionLabelNotEmpty()
    {
        $expectedResult = 'test';
        $this->setProtectedValue($this->filterType, 'optionLabel', $expectedResult);
        $this->assertEquals($expectedResult, $this->filterType->getOptionLabel());
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('', $this->filterType->getOptionLabel());
    }

    /**
     * testIsFilterObjectValid
     *
     */
    public function testIsFilterObjectValid()
    {
        $this->assertTrue($this->filterType->isFilterObjectValid($this->filter));
    }

    /**
     * testIsDateValid
     *
     */
    public function testIsDateValid()
    {
        $this->assertFalse($this->filterType->isDateValid(array()));
        $this->assertFalse($this->filterType->isDateValid('2014-01-01'));
        $this->assertFalse($this->filterType->isDateValid('15/15/2014'));
        $this->assertTrue($this->filterType->isDateValid('01/01/2014'));
    }
}
