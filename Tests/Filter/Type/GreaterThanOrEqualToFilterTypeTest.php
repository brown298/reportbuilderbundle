<?php
namespace Filter\Type;

use Phake;

/**
 * Class GreaterThanOrEqualToFilterTest
 *
 * @package Filter\Type
 * @author John Brown <john.brown@partnerweekly.com>
 */
class GreaterThanOrEqualToFilterTypeTest extends GreaterThanFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\GreaterThanOrEqualToFilterType';

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->gte(Phake::anyParameters())->thenReturn('comparison');
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is greater than or equal to', $this->filterType->getOptionLabel());
    }
} 