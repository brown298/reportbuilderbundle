<?php
namespace Filter\Type;

use Phake;

/**
 * Class EmptyFilterTypeTest
 *
 * @package Filter\Type
 */
class EmptyFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\EmptyFilterType';

    /**
     * applyFilterProvider
     *
     * @return array|mixed
     */
    public function applyFilterProvider()
    {
        return array(
            array(true, 'testProperty IS NULL'),
            array(false, 'testProperty IS NULL'),
        );
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is empty', $this->filterType->getOptionLabel());
    }
} 