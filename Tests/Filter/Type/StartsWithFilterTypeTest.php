<?php
namespace Filter\Type;

use Phake;

/**
 * Class StartsWithFilterTypeTest
 *
 * @package Filter\Type
 * @author John Brown <john.brown@partnerweekly.com>
 */
class StartsWithFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\StartsWithFilterType';

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->like(Phake::anyParameters())->thenReturn('comparison');
    }


    /**
     * applyFilterProvider
     *
     * @return array|mixed
     */
    public function applyFilterProvider()
    {
        $this->setUp();
        return array(
            array(true,  'comparison'),
            array(false, 'comparison'),
        );
    }

    /**
     * testApplyFilter
     * @dataProvider applyFilterProvider
     */
    public function testApplyFilter($having, $message)
    {
        $filter = 'filter';
        Phake::when($this->filter)->getValue()->thenReturn($filter);

        parent::testApplyFilter($having, $message);
        Phake::verify($this->filter)->getValue();
        Phake::verify($this->expr)->literal($filter.'%');
        Phake::verify($this->expr)->like('testProperty', $this->literal);
    }

    /**
     * isFilterObjectValidProvider
     *
     */
    public function isFilterObjectValidProvider()
    {
        return array(
            array('test', true),
            array('', false),
            array(null, false),
        );
    }

    /**
     * testIsFilterObjectValid
     *
     * @dataProvider isFilterObjectValidProvider
     */
    public function testIsFilterObjectValid($value, $exptectedResult)
    {
        Phake::when($this->filter) ->getValue()->thenReturn($value);
        $result = $this->filterType->isFilterObjectValid($this->filter);

        if ($exptectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('key', $this->filterType->getOptionLabel());
    }
}