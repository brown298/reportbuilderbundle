<?php
namespace Filter\Type;

use Phake;

/**
 * Class NotBetweenDatetimesFilterTypeTest
 *
 * @package Filter\Type
 */
class NotBetweenDatetimesFilterTypeTest extends NotBetweenDatesFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\NotBetweenDatetimesFilterType';

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is not between', $this->filterType->getOptionLabel());
    }


}