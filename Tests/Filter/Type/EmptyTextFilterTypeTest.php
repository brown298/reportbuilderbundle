<?php
namespace Filter\Type;

use Phake;
/**
 * Class EmptyTextFilterTypeTest
 *
 * @package Filter\Type
 */
class EmptyTextFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\EmptyTextFilterType';

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->orX(Phake::anyParameters())->thenReturn('testProperty IS NULL');
    }

    /**
     * applyFilterProvider
     *
     * @return array|mixed
     */
    public function applyFilterProvider()
    {
        return array(
            array(true, 'testProperty IS NULL'),
            array(false, 'testProperty IS NULL'),
        );
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is empty', $this->filterType->getOptionLabel());
    }
} 