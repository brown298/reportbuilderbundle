<?php
namespace Filter\Type;

use Phake;

/**
 * Class ContainsFilterTypeTest
 *
 * @package Filter\Type
 * @author John Brown <john.brown@partnerweekly.com>
 */
class EqualToFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\EqualToFilterType';

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->eq(Phake::anyParameters())->thenReturn('comparison');
    }

    /**
     * applyFilterProvider
     *
     * @return mixed
     */
    function applyFilterProvider()
    {
        return array(
            array(true,  'comparison'),
            array(false, 'comparison'),
        );
    }

    /**
     * isFilterObjectValidProvider
     *
     */
    public function isFilterObjectValidProvider()
    {
        return array(
            array('test', true),
            array('', false),
            array(null, false),
            array('01', true),
        );
    }

    /**
     * testIsFilterObjectValid
     *
     * @dataProvider isFilterObjectValidProvider
     */
    public function testIsFilterObjectValid($value, $exptectedResult)
    {
        Phake::when($this->filter)->getValue()->thenReturn($value);
        Phake::when($this->filter)->getSecondValue()->thenReturn($value);
        $result = $this->filterType->isFilterObjectValid($this->filter);

        if ($exptectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('equals', $this->filterType->getOptionLabel());
    }
} 