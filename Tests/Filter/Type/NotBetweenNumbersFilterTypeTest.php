<?php
namespace Filter\Type;

use Phake;

/**
 * Class NotBetweenNumbersFilterTypeTest
 *
 * @package Filter\Type
 */
class NotBetweenNumbersFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\NotBetweenNumbersFilterType';

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->not(Phake::anyParameters())->thenReturn('comparison');
    }

    /**
     * applyFilterProvider
     *
     * @return mixed
     */
    function applyFilterProvider()
    {
        return array(
            array(true,  'comparison'),
            array(false, 'comparison'),
        );
    }

    /**
     * isFilterObjectValidProvider
     *
     */
    public function isFilterObjectValidProvider()
    {
        return array(
            array('test', false),
            array('', false),
            array(null, false),
            array('01', true),
        );
    }

    /**
     * testIsFilterObjectValid
     *
     * @dataProvider isFilterObjectValidProvider
     */
    public function testIsFilterObjectValid($value, $exptectedResult)
    {
        Phake::when($this->filter)->getValue()->thenReturn($value);
        Phake::when($this->filter)->getSecondValue()->thenReturn($value);
        $result = $this->filterType->isFilterObjectValid($this->filter);

        if ($exptectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is not between', $this->filterType->getOptionLabel());
    }
}