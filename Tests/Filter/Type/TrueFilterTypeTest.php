<?php
namespace Filter\Type;

use Phake;

/**
 * Class TrueFilterTypeTest
 *
 * @package Filter\Type
 */
class TrueFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\TrueFilterType';

    /**
     * applyFilterProvider
     *
     * @return array|mixed
     */
    public function applyFilterProvider()
    {
        return array(
            array(true, 'testProperty <> false'),
            array(false, 'testProperty <> false'),
        );
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is true', $this->filterType->getOptionLabel());
    }
} 