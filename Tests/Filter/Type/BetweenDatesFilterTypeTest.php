<?php
namespace Filter\Type;

use Phake;

/**
 * Class BetweenDatesFilterTypeTest
 *
 * @package Filter\Type
 */
class BetweenDatesFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\BetweenDatesFilterType';

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->between(Phake::anyParameters())->thenReturn('comparison');
    }

    /**
     * applyFilterProvider
     *
     * @return mixed
     */
    function applyFilterProvider()
    {
        return array(
            array(true,  'comparison'),
            array(false, 'comparison'),
        );
    }

    /**
     * testApplyFilter
     * @dataProvider applyFilterProvider
     */
    public function testApplyFilter($having, $message)
    {
        parent::testApplyFilter($having, $message);
        Phake::verify($this->qb, Phake::atLeast(2))->setParameter($this->anything(), Phake::capture($date));
        $this->assertInstanceOf('\DateTime', $date);
    }

    /**
     * isFilterObjectValidProvider
     *
     */
    public function isFilterObjectValidProvider()
    {
        return array(
            array('test', false),
            array('', false),
            array(null, false),
            array('01/01/2010', true),
        );
    }

    /**
     * testIsFilterObjectValid
     *
     * @dataProvider isFilterObjectValidProvider
     */
    public function testIsFilterObjectValid($value, $exptectedResult)
    {
        Phake::when($this->filter)->getValue()->thenReturn($value);
        Phake::when($this->filter)->getSecondValue()->thenReturn($value);
        $result = $this->filterType->isFilterObjectValid($this->filter);

        if ($exptectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals(strtolower('is between'), $this->filterType->getOptionLabel());
    }
} 