<?php
namespace Filter\Type;

use Phake;

/**
 * Class BetweenDatetimesFilterTypeTest
 *
 * @package Filter\Type
 */
class BetweenDatetimesFilterTypeTest extends BetweenDatesFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\BetweenDatetimesFilterType';

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals(strtolower('is between'), $this->filterType->getOptionLabel());
    }
}