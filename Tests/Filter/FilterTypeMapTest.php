<?php
namespace Filter;

use Brown298\ReportBuilderBundle\Filter\FilterTypeMap;
use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class FilterTypeMapTest
 * @package Filter
 */
class FilterTypeMapTest extends AbstractTest
{
    public function testGetKeysForColumn()
    {
        $string = array(
            'contains', 'equal_to', 'not_equal_to', 'starts_with', 'ends_with',
            'empty_text', 'not_empty_text',
        );
        $integer = array(
            'equal_to_number', 'not_equal_to_number', 'greater_than',
            'greater_than_or_equal_to', 'less_than', 'less_than_or_equal_to',
            'between_numbers', 'not_between_numbers', 'empty_number',
            'not_empty_number',
        );
        $date = array(
            'on_date', 'on_or_after_date', 'on_or_before_date',
            'between_dates', 'not_between_dates', 'empty', 'not_empty',
        );
        $count = array(
            'equal_to_number', 'not_equal_to_number', 'greater_than',
            'greater_than_or_equal_to', 'less_than', 'less_than_or_equal_to',
            'between_numbers', 'not_between_numbers',
        );

        $this->assertEquals($string, FilterTypeMap::getKeysForColumn('string'));
        $this->assertEquals($string, FilterTypeMap::getKeysForColumn('text'));
        $this->assertEquals($integer, FilterTypeMap::getKeysForColumn('integer'));
        $this->assertEquals($integer, FilterTypeMap::getKeysForColumn('decimal'));
        $this->assertEquals($date, FilterTypeMap::getKeysForColumn('date'));
        $this->assertNull(FilterTypeMap::getKeysForColumn('gobbledy-gook'));
        $this->assertEquals($count, FilterTypeMap::getKeysForCount());
    }
}