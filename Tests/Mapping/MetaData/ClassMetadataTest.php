<?php
namespace Mapping\MetaData;

use Brown298\ReportBuilderBundle\Mapping\MetaData\Annotation\ClassMetadata;
use Phake;
use Brown298\TestExtension\Test\AbstractTest;

/**
 * Class ClassMetadataTest
 * @package Mapping\MetaData
 */
class ClassMetadataTest extends AbstractTest
{
    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata
     */
    protected $treeMetadata;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertyMetadataInterface
     */
    protected $propertyMetadata;

    /**
     * @Mock
     * @var \Doctrine\Common\Annotations\FileCacheReader
     */
    protected $annotationReader;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface
     */
    protected $security;

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface
     */
    protected $classMetadata;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\ClassMetadataFactory
     */
    protected $classMetadataFactory;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\PropertyMetadataFactory
     */
    protected $propertyMetadataFactory;

    /**
     * @var string
     */
    protected $class = 'Mapping\MetaData\ClassMetadataTest';

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->treeMetadata)->getAnnotationReader()->thenReturn($this->annotationReader);

        $this->classMetadata = Phake::partialMock('Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface',
            $this->classMetadataFactory, $this->propertyMetadataFactory, $this->security, $this->treeMetadata, $this->propertyMetadata);
        $this->classMetadata->init($this->class);
    }

    /**
     * testCreate
     *
     * ensures we don't get an exception in the constructor
     */
    public function testCreate()
    {
        $this->assertInstanceOf('Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface', $this->classMetadata);
    }

    /**
     * testCreateNonExisting
     * @expectedException \Brown298\ReportBuilderBundle\Mapping\Exception\CreationException
     */
    public function testCreateNonExisting()
    {
        $this->classMetadata = new ClassMetadata($this->classMetadataFactory, $this->propertyMetadataFactory,
            $this->security, $this->treeMetadata, $this->propertyMetadata);
        $this->classMetadata->init('asdf');
    }
} 