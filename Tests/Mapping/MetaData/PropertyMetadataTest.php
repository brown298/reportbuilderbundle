<?php
namespace Mapping\MetaData;

use Brown298\ReportBuilderBundle\Mapping\MetaData\Annotation\PropertyMetadata;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\FileCacheReader;
use Doctrine\ORM\Mapping\Column;
use Phake;
use Brown298\TestExtension\Test\AbstractTest;

/**
 * Class PropertyMetadataTest
 * @package Mapping\MetaData
 */
class PropertyMetadataTest extends AbstractTest
{

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata
     */
    protected $treeMetadata;

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertyMetadataInterface
     */
    protected $propertyMetadata;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface
     */
    protected $classMetadata;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\ClassMetadataFactory
     */
    protected $classMetadataFactory;

    /**
     * @Mock
     * @var \Doctrine\Common\Annotations\FileCacheReader
     */
    protected $annotationReader;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface
     */
    protected $security;

    /**
     * @var string
     */
    protected $class = 'Brown298\ReportBuilderBundle\Tests\Entity\TestEntity';

    /**
     * @var string
     */
    protected $property = 'id';

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->classMetadata)->getClass()->thenReturn($this->class);
        Phake::when($this->treeMetadata)->getAnnotationClass(Phake::anyParameters())->thenReturn('Doctrine\ORM\Mapping\Column');
        Phake::when($this->treeMetadata)->getAnnotationReader()->thenReturn($this->annotationReader);
        Phake::when($this->annotationReader)->getPropertyAnnotations(Phake::anyParameters())->thenReturn(array(
            new Column(),
        ));

        $this->propertyMetadata = new PropertyMetadata($this->classMetadataFactory, $this->classMetadata, $this->property, $this->security);
    }

    /**
     * testCreate
     *
     * ensures we don't get an exception in the constructor
     */
    public function testCreate()
    {
        $this->assertInstanceOf('Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertyMetadataInterface', $this->propertyMetadata);
    }

} 