<?php
namespace Mapping\MetaData;

use Phake;
use Brown298\TestExtension\Test\AbstractTest;

/**
 * Class TreeMetadataTest
 * @package Mapping\MetaData
 */
class TreeMetadataTest extends AbstractTest
{

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata
     */
    protected $treeMetadata;

    /**
     * @Mock
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    protected $translator;

    /**
     * @Mock
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();

        $this->treeMetadata = Phake::partialMock('Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata');
    }

    /**
     * testCreate
     *
     * ensures we don't get an exception in the constructor
     */
    public function testCreate()
    {
        $this->assertInstanceOf('Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata', $this->treeMetadata);
    }

    /**
     * testGetSetTranslator
     */
    public function testGetSetTranslator()
    {
        $this->assertNull($this->treeMetadata->getTranslator());

        $this->treeMetadata->setTranslator($this->translator);
        $this->assertEquals($this->translator, $this->treeMetadata->getTranslator());
    }

    /**
     * testGetSetEm
     */
    public function testGetSetEm()
    {
        $this->assertNull($this->treeMetadata->getEm());
        $this->assertFalse($this->treeMetadata->hasEm());

        $this->treeMetadata->setEm($this->em);
        $this->assertEquals($this->em, $this->treeMetadata->getEm());
        $this->assertTrue($this->treeMetadata->hasEm());
    }

    /**
     * testGetSetDepthLimit
     */
    public function testGetSetDepthLimit()
    {
        $this->assertEquals(1, $this->treeMetadata->getDepthLimit());

        $this->treeMetadata->setDepthLimit(10);
        $this->assertEquals(10, $this->treeMetadata->getDepthLimit());
    }

    /**
     * testSetDepthLimitNonInt
     *
     * @expectedException InvalidArgumentException
     */
    public function testSetDepthLimitNonInt()
    {
        $this->treeMetadata->setDepthLimit('11c');
    }

    /**
     * testSetDepthLimitNegative
     *
     * @expectedException InvalidArgumentException
     */
    public function testSetDepthLimitNegative()
    {
        $this->treeMetadata->setDepthLimit(-11);
    }

    /**
     * testFindDoctrineClassMetadataMissingEm
     * @expectedException Exception
     */
    public function testFindDoctrineClassMetadataMissingEm()
    {
        $this->treeMetadata->findDoctrineClassMetadata('a');
    }
} 