<?php
namespace Mapping\Annotation;

use Brown298\ReportBuilderBundle\Mapping\Annotation\Ignored;
use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class IgnoredTest
 * @package Mapping\Annotation
 */
class IgnoredTest extends AbstractTest
{

    /**
     * testGetSet
     */
    public function testGetSet()
    {
        $entity = 'testEntity';
        $path   = 'testPath';
        $args   = array('entity' => $entity, 'path' => $path);

        $ignored = new Ignored($args);

        $this->assertEquals($entity, $ignored->getEntity());
        $this->assertEquals($path, $ignored->getPath());
    }

    /**
     * testParse
     *
     * @dataProvider parseDataProvider
     *
     * @param $path
     * @param $expectedResult
     */
    public function testParse($path, $expectedResult)
    {
        $entity  = 'testEntity';
        $args   = array('entity' => $entity, 'path' => $path);

        $ignored = new Ignored($args);

        $this->assertEquals($expectedResult, $ignored->getPaths());
    }

    /**
     * @return array
     */
    public function parseDataProvider()
    {
        return array(
            array(
                'asdf',
                array('asdf')
            ),
            array(
                'bizz,buzz',
                array('bizz', 'buzz')
            ),
            array(
                'bizz , buzz',
                array('bizz', 'buzz')
            ),
            array(
                "foo , bar\n ,qwerty",
                array('foo', 'bar', 'qwerty')
            ),
            array(
                "     *     created_by.user_name, created_by.supervisor.user_name,\n *     updated_by.user_name, updated_by.supervisor.user_name\n * ",
                array('created_by.user_name', 'created_by.supervisor.user_name', 'updated_by.user_name', 'updated_by.supervisor.user_name')
            ),
        );
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testPathWithoutEntityThrowsException()
    {
        $path   = 'testPath';
        $args   = array('path' => $path);

        $ignored = new Ignored($args);
    }

    public function testParsePathsEmpty()
    {
        $ignored = new Ignored(array());

        $this->assertNull($ignored->getEntity());
        $this->assertNull($ignored->getPath());
        $this->assertFalse($ignored->hasFullPath());
    }

    /**
     * testParsePaths
     *
     * @dataProvider parseDataProvider
     *
     * @param $path
     * @param $expectedResult
     */
    public function testParsePaths($path, $expectedResult)
    {
        $ignored = new Ignored(array());

        $result = $this->callProtected($ignored, 'parsePaths', array($path));

        $this->assertEquals($expectedResult, $result);
    }
}
