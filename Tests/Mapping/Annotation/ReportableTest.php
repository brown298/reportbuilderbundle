<?php
namespace Mapping\Annotation;

use Brown298\ReportBuilderBundle\Mapping\Annotation\Reportable;
use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class ReportableTest
 * @package Mapping\Annotation
 */
class ReportableTest extends AbstractTest
{

    /**
     * testGetSetEmpty
     */
    public function testGetSetEmpty()
    {
        $reportable = new Reportable(array());

        $this->assertNull($reportable->getLabel());
        $this->assertFalse($reportable->hasLabel());
    }

    /**
     * testGetSet
     */
    public function testGetSet()
    {
        $label = 'test';

        $reportable = new Reportable(array('label' => $label));

        $this->assertTrue($reportable->hasLabel());
        $this->assertEquals($label, $reportable->getLabel());
    }
} 