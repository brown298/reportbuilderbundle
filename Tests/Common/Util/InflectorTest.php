<?php
namespace Common\Util;

use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;
use \Brown298\ReportBuilderBundle\Common\Util\Inflector;

/**
 * Class InflectorTest
 * @package Common\Util
 */
class InflectorTest extends AbstractTest
{

    /**
     * @var Inflector
     */
    protected $inflector;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->inflector = new Inflector();
    }

    /**
     * @param $string
     * @param $expected
     *
     * @dataProvider labels
     */
    public function testLabelize($string, $expected)
    {
        $this->assertSame($expected, Inflector::labelize($string));
    }

    public function labels()
    {
        return array(
            array('Test', 'Test'),
            array('Some\Test', 'Test'),
            array('SomethingParent\Some\Test', 'Test'),
            array('SomethingParent\\Some\\Test', 'Test'),
            array('SomethingParent\Some\TestSomething', 'Test Something'),
            array('TestWithId', 'Test With'),
            array('ModelName', 'Model Name'),
            array('Namespace\ModelName', 'Model Name'),
            array('MyBundle:ModelName', 'Model Name'),
            array('camelCased', 'Camel Cased'),
            array('underscored_words', 'Underscored Words'),
            array('property_id', 'Property'),
        );
    }

    /**
     * @param $string
     * @param $expected
     *
     * @dataProvider words
     */
    public function testWords($string, $expected)
    {
        $this->assertSame($expected, Inflector::getWords($string));
    }

    /**
     * @return array
     */
    public function words()
    {
        return array(
            array('Test', array('Test')),
            array('TestSome', array('Test', 'Some')),
            array('SomeTestMore', array('Some','Test', 'More')),
        );
    }

    /**
     * @param $string
     * @param $expected
     *
     * @dataProvider namespaces
     */
    public function testGetClass($string, $expected)
    {
        $this->assertSame($expected, $this->inflector->getClass($string));
    }

    public function namespaces()
    {
        return array(
            array('Test', 'Test'),
            array('Some\Test', 'Test'),
            array('SomethingParent\Some\Test', 'Test'),
            array('SomethingParent\\Some\\Test', 'Test'),
            array('SomethingParent\Some\TestSomething', 'TestSomething'),
        );
    }

    /**
     * @param $bytes
     * @param $decimals
     * @param $expected
     *
     * @dataProvider getHumanBytesTests
     */
    public function testGetHumanBytes($bytes, $decimals, $expected)
    {
        $this->assertSame($expected, $this->inflector->getHumanBytes($bytes, $decimals));
    }

    public function getHumanBytesTests()
    {
        return array(
            array(1024, 0, '1 KB'),
            array(1024*1024, 0, '1 MB'),
            array(1024*1024*1024, 0, '1 GB'),
            array(1024*1024*1024*1024, 0, '1 TB'),
            array(1024, 4, '1.0000 KB'),
            array(1024*1024*1024*1024*5, 0, '5 TB'),
            array(1024*1024*1024*1024*5.23984, 6, '5.239840 TB'),
            array(348721800, 0, '333 MB'),
        );
    }

    /**
     * @param string $name
     * @param string $expected
     *
     * @dataProvider trimConstants
     */
    public function testConvertConstWithTrim($name, $expected)
    {
        $this->assertSame($expected, $this->inflector->convertConst($name));
    }

    /**
     * @return array
     */
    public function trimConstants()
    {
        return array(
            array('Some String', 'SOME_STRING'),
            array('Some String 2', 'SOME_STRING_2'),
            array('Some/String 2', 'SOME/STRING_2'),
            array('Some/String 2 that is really really really really really long', 'SOME/STRING_2_THAT_IS_REALLY_REALLY_REAL'),
        );
    }

    /**
     * @param $name
     * @param $trim
     * @param $expected
     *
     * @dataProvider truncateConstants
     */
    public function testConvertToConstWithTruncate($name, $trim, $expected)
    {
        $this->assertSame($expected, $this->inflector->convertToConst($name, $trim));
    }

    /**
     * @return array
     */
    public function truncateConstants()
    {
        return array(
            array('Some String', 40, 'SOME_STRING'),
            array('Some String 2', 40, 'SOME_STRING_2'),
            array('Some String 2', 10, 'SOME_STRIN'),
            array('Some/String 2', 40, 'SOME_STRING_2'),
            array('Some/String 2 that is really really really really really long', 40, 'SOME_STRING_2_THAT_IS_REALLY_REALLY_REAL'),
        );
    }

    public function testLabelizeCallback()
    {
        $matches        = array('test', 'asdf');
        $expectedResult = 'TEST';
        $result         = $this->inflector->labelizeCallback($matches);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * testGetNameforClassObject
     */
    public function testGetNameforClassObject()
    {
        $expectedResult = 'brown298_reportbuilderbundle_common_util_inflector';
        $this->assertEquals($expectedResult, $this->inflector->getNameFromClass($this->inflector));
    }
} 