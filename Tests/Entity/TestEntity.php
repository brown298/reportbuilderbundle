<?php
namespace Brown298\ReportBuilderBundle\Tests\Entity;

use Doctrine\ORM\Mapping as ORM;
use Brown298\ReportBuilderBundle\Mapping\Annotation as Report;

/**
 * Class TestEntity
 *
 * @ORM\Entity()
 */
class TestEntity
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
} 