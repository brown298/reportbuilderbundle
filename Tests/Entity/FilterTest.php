<?php
namespace Brown298\ReportBuilderBundle\Tests\Entity;

use Phake;

/**
 * Class FilterTest
 * @package Brown298\ReportBuilderBundle\Tests\Entity
 */
class FilterTest extends AbstractReportEntityTest
{
    /**
     * @var string
     */
    protected $class = 'Filter';

    /**
     * testIsValidNoPath
     */
    public function testIsValidNoPath()
    {
        $this->entity->setPath(null);
        $this->entity->setFunction(null);

        $this->assertFalse($this->entity->isValid($this->context));

        Phake::verify($this->context)->addViolationAt($this->anything(), 'Please fill in the path field');
        Phake::verify($this->context)->getPropertyPath();
    }

    /**
     * testIsValidNoType
     */
    public function testIsValidNoType()
    {
        $this->entity->setPath('first_name');
        $this->entity->setFunction(null);
        $this->entity->setType(null);

        $this->assertFalse($this->entity->isValid($this->context));

        Phake::verify($this->context)->addViolationAt($this->anything(), 'Please choose a filter type');
        Phake::verify($this->context)->getPropertyPath();
    }

    /**
     * testIsValidBadFilterType
     */
    public function testIsValidBadFilterType()
    {
        $this->entity->setPath('first_name');
        $this->entity->setFunction(null);
        $this->entity->setType('equal_to');
        $this->entity->setValue('');

        $this->assertFalse($this->entity->isValid($this->context));

        Phake::verify($this->context)->addViolationAt($this->anything(), 'Please enter a value');
        Phake::verify($this->context)->getPropertyPath();
    }

    /**
     * testIsValidGoodFilterType
     */
    public function testIsValidGoodFilterType()
    {
        $this->entity->setPath('first_name');
        $this->entity->setFunction(null);
        $this->entity->setType('equal_to');
        $this->entity->setValue('asdf');

        $this->assertTrue($this->entity->isValid($this->context));
    }

    /**
     * testMoreGettersAndSetters
     */
    public function testMoreGettersAndSetters()
    {
        parent::testMoreGettersAndSetters();

        // Type
        $type = 'true';
        $this->entity->setType($type);
        $this->assertSame($type, $this->entity->getType());

        // Value
        $value = 'asdfiuekirfv';
        $this->entity->setValue($value);
        $this->assertSame($value, $this->entity->getValue());

        // Second value
        $value = '2asdciolkusdfiuekirfv';
        $this->entity->setSecondValue($value);
        $this->assertSame($value, $this->entity->getSecondValue());
    }
}