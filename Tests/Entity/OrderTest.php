<?php
namespace Brown298\ReportBuilderBundle\Tests\Entity;

/**
 * Class OrderTest
 * @package Brown298\ReportBuilderBundle\Tests\Entity
 */
class OrderTest extends AbstractReportEntityTest
{
    /**
     * @var string
     */
    protected $class = 'Order';

    /**
     * testToString
     */
    public function testToString()
    {
        $entityName = sprintf($this->classNameTemplate, $this->class);
        $entity     = new $entityName;
        $path = 'first_name';
        $entity->setPath($path);
        $entity->setFunction(null);
        $entity->setAscending(false);

        $this->assertSame((string) $entity, $path.', descending');
        $entity->setPath(null);
        $entity->setFunction($this->count);
        $entity->setAscending(true);
        $this->assertSame((string) $entity, 'Count, ascending');
    }
}