<?php
namespace Brown298\ReportBuilderBundle\Mapping\Exception;

use \Exception;

/**
 * Class CreationException
 * @package Brown298\ReportBuilderBundle\Mapping\Exception
 */
class CreationException extends Exception
{

} 