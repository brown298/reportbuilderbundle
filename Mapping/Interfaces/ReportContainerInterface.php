<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;

/**
 * Interface ReportContainerInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface ReportContainerInterface
{
    /**
     * getPermissionModifier
     *
     * @param mixed $user
     * @return string
     */
    public function getPermissionModifier($user);

    /**
     * Get report
     *
     * @return Report
     */
    public function getReport();

    /**
     * Get parameter form
     *
     * @return null
     */
    public function getParameterForm();

    /**
     * getKey
     *
     * @return string
     */
    public function getKey();

} 