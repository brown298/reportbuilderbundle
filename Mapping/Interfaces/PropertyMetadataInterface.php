<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;
use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;
use Doctrine\ORM\EntityManager;

/**
 * Interface PropertyMetadataInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface PropertyMetadataInterface extends MetadataInterface
{
    /**
     * @return mixed
     */
    public function init();

    /**
     * @return mixed
     */
    public function getPath();

    /**
     * @return mixed
     */
    public function isBase();

    /**
     * createSelection
     *
     * @return PropertySelection
     */
    public function createSelection();

    /**
     * hasAssociatedClass
     *
     * @return bool
     */
    public function hasAssociatedClass();

    /**
     * getAssociatedClassMeta
     *
     * @return ClassMetadata|false
     */
    public function getAssociatedClassMetadata();

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getClass();

    /**
     * isIgnored
     *
     * @return boolean
     */
    public function isIgnored();

    /**
     * isSecured
     *
     * @return boolean
     */
    public function isSecured();

    /**
     * Get depth
     *
     * @return integer
     */
    public function getDepth();

    /**
     * Recursively find path
     *
     * @return array
     */
    public function getPathPieces();

    /**
     * getClassMeta
     *
     * @return ClassMetadata
     */
    public function getClassMetadata();

    /**
     * getLabelPathPrefix
     *
     * @return string
     */
    public function getLabelPathPrefix();

    /**
     * Recursively find path
     *
     * @return array
     */
    public function getLabelPathPieces();

    /**
     * @return mixed
     */
    public function isChildFreeParentInTree();

    /**
     * @return mixed
     */
    public function getFilterTypes();

    /**
     * @return mixed
     */
    public function findFilterTypes();
}