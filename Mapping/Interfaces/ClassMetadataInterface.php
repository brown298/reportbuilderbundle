<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;
use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;


/**
 * Interface ClassMetadataInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface ClassMetadataInterface extends MetadataInterface
{
    /**
     * @param string $class
     * @return mixed
     */
    public function init($class);

    /**
     * @return mixed
     */
    public function getClass();

    /**
     * Get parent property
     *
     * @return PropertyMetadata
     */
    public function getParentPropertyMetadata();

    /**
     * getSelections
     *
     * @param  string $propertyRetrievalMethod
     * @param null $context
     * @throws \InvalidArgumentException
     * @return PropertySelection[]
     */
    public function getSelections($propertyRetrievalMethod = null, $context = null);

    /**
     * getPropertyMetaDatasForTree
     *
     * @return PropertyMetadata[]
     */
    public function getPropertyMetadatasForTree();

    /**
     * getPropertyMetadatas
     *
     * @return PropertyMetadata[]
     */
    public function getPropertyMetadatas();

    /**
     * hasPropertyMetadatas
     *
     * @return bool
     */
    public function hasPropertyMetadatas();

    /**
     * @param $name
     * @return mixed
     */
    public function getPropertyMetadata($name);

    /**
     * @param null $context
     * @return mixed
     */
    public function getPropertyMetadatasForTreeOnlyFields($context = null);
}