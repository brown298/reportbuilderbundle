<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Interface TranslatableInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface TranslatableInterface
{
    /**
     * setTranslator
     *
     * @param TranslatorInterface $translator
     * @return null
     */
    public function setTranslator(TranslatorInterface $translator);

    /**
     * getTranslator
     *
     * @return Translator
     */
    public function getTranslator();
} 