<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;

/**
 * Interface ReportAssetInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface ReportAssetInterface
{
    /**
     * getPath
     *
     * @return string
     */
    public function getPath();

    /**
     * getFunction
     *
     * @return string
     */
    public function getFunction();

} 