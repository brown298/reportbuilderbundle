<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;
use Doctrine\ORM\EntityManager;

/**
 * Interface YmlReaderInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface YmlReaderInterface
{
    /**
     * @param $reader
     * @return mixed
     */
    public function setMetadataReader($reader);

}