<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Interface FilterTypeInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface FilterTypeInterface
{
    /**
     * @param QueryBuilder $qb
     * @param Filter $filter
     * @param $property
     * @param bool $having
     * @return mixed
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false);

    /**
     * Returns the first unused parameter index. Always returns a unique index,
     * even setParameter has not yet been called on the query builder object
     *
     * @param QueryBuilder $qb
     * @return integer
     */
    public function createQbParameterKey(QueryBuilder $qb);

    /**
     * isDateValid
     *
     * @param string $date
     * @return bool
     */
    public function isDateValid($date);

    /**
     * isFilterObjectValid
     *
     * @param Filter $filter
     * @return bool
     */
    public function isFilterObjectValid(Filter $filter);

    /**
     * getValidationErrors
     *
     * @return array
     */
    public function getValidationErrors();

    /**
     * addValidationError
     *
     * @param string $field
     * @param string $message
     * @return null
     */
    public function addValidationError($field, $message);

    /**
     * getFormTemplate
     *
     * @return string
     */
    public function getFormTemplate();

    /**
     * getOptionLabel
     *
     * @return string
     */
    public function getOptionLabel();

    /**
     * getLabel
     *
     * @return string
     */
    public function getLabel();

    /**
     * getKey
     *
     * @return string
     */
    public function getKey();
} 