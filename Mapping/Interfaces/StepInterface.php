<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;

/**
 * Interface StepInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface StepInterface
{

    /**
     * @return mixed
     */
    public function getLabel();

    /**
     * @return mixed
     */
    public function isFinished();

    /**
     * @return mixed
     */
    public function getKey();

} 