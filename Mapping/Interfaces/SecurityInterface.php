<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;

use Brown298\ReportBuilderBundle\Mapping\Annotation\Secure;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportContainerInterface;

/**
 * Class SecurityInterface
 *
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface SecurityInterface
{

    /**
     * Returns true if the passed class is not being restricted for the current
     * user. Reads the Secure annotation on the entity.
     *
     * @param string $class
     * @return bool
     */
    public function isViewingAllowed($class);

    /**
     * Throws an exception if the current user is not allowed to perform the
     * passed action on the current report's type (system, built, others'
     * built).
     *
     * @param  string $action
     * @param  string $modifier
     * @throws AccessDeniedException
     * @return null
     */
    public function checkPermissions($action, $modifier = null);

    /**
     * Returns false if the current user is not allowed to perform the passed
     * action on the current report's type (system, built, others' built).
     *
     * @param  string $action
     * @param  string $modifier
     * @return boolean
     */
    public function hasPermissions($action, $modifier = null);

    /**
     * Throws an exception if the current report contains classes that the
     * current user is not allowed to view.
     *
     * @throws AccessDeniedException
     * @return null
     */
    public function checkReportClassPermissions();

    /**
     * getName
     *
     * gets the name of the current service
     *
     * @return string
     */
    public function getName();

    /**
     * setReportContainer
     *
     * @param ReportContainerInterface $reportContainer
     * @return null
     */
    public function setReportContainer(ReportContainerInterface $reportContainer = null);

    /**
     * getCanEditOthers
     *
     * determines if current user can edit other user's reports
     * @return bool
     */
    public function getCanEditOthers();

    /**
     * checkSecuredAnnotation
     *
     * checks the user against a secure annotation
     *
     * @param Secure $secure
     *
     * @return boolean
     */
    public function checkSecuredAnnotation(Secure $secure);
} 