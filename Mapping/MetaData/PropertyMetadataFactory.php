<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData;

use Brown298\ReportBuilderBundle\Mapping\Exception\CreationException;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\AnnotationReaderInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\YmlReaderInterface;
use Brown298\ReportBuilderBundle\Mapping\MetaData\Annotation\ClassMetadata;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManager;

/**
 * Class PropertyMetadataFactory
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
class PropertyMetadataFactory
{

    /**
     * @var string
     */
    protected $propertyMetaClass = 'Brown298\ReportBuilderBundle\Mapping\MetaData\Annotation\PropertyMetadata';

    /**
     * @var
     */
    protected $metadataReader;

    /**
     * @var TreeMetadata
     */
    protected $treeMetadata;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     * @param TreeMetadata $treeMetadata
     * @param null $metadataReader
     * @param null $propertyMetadataClassName
     */
    public function __construct(TreeMetadata $treeMetadata, $metadataReader = null, $propertyMetadataClassName = null)
    {
        if ($propertyMetadataClassName != null) {
            $this->propertyMetaClass = $propertyMetadataClassName;
        }

        if ($metadataReader) {
            $this->metadataReader = $metadataReader;
        }
        $this->treeMetadata = $treeMetadata;
    }

    /**
     * @param ClassMetadataFactory   $classMetadataFactory
     * @param ClassMetadataInterface $classMetadata
     * @param $name
     * @return mixed
     */
    public function generate(ClassMetadataFactory $classMetadataFactory, ClassMetadataInterface $classMetadata, $name)
    {
        $propertyMetadataClass = $this->propertyMetaClass;
        $propertyMetadata = new $propertyMetadataClass($classMetadataFactory, $classMetadata, $name, $this->treeMetadata->getSecurityService(), $this->treeMetadata);

        $propertyMetadata->setMetadataReader($this->metadataReader);

        try {
            $propertyMetadata->init();
        } catch (CreationException $ex) {
            return null;
        }

        return $propertyMetadata;
    }


    /**
     * setSecurityService
     *
     * @param SecurityInterface $security
     * @return null
     */
    public function setSecurityService(SecurityInterface $security)
    {
        $this->security = $security;
    }

    /**
     * @return mixed
     */
    public function getSecurityService()
    {
        return $this->security;
    }

    /**
     * @return mixed
     */
    public function getMetadataReader()
    {
        return $this->metadataReader;
    }

    /**
     * @param Reader $metadataReader
     */
    public function setMetadataReader(Reader $metadataReader)
    {
        $this->metadataReader = $metadataReader;
    }
}