<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData;

/**
 * Class PropertySelection
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
class PropertySelection
{
    /**
     * @var string
     */
    public $path;

    /**
     * @var string
     */
    public $func;

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $labelPathPrefix;

    /**
     * @var PropertySelection[]
     */
    protected $children = array();

    /**
     * setPath
     *
     * @param  string $path
     * @return null
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * getPath
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * isBase
     *
     * determine if this is from the base element
     *
     * @return type
     */
    public function isBase()
    {
        return $this->path && strpos($this->path,'.') === false;
    }

    /**
     * setFunc
     *
     * @param  string $func
     * @return null
     */
    public function setFunc($func)
    {
        $this->func = $func;
    }

    /**
     * getFunc
     *
     * @return string
     */
    public function getFunc()
    {
        return $this->func;
    }

    /**
     * setLabel
     *
     * @param  string $label
     * @return null
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * getLabel
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * setLabelPathPrefix
     *
     * @param  string $labelPathPrefix
     * @return null
     */
    public function setLabelPathPrefix($labelPathPrefix)
    {
        $this->labelPathPrefix = $labelPathPrefix;
    }

    /**
     * getLabelPathPrefix
     *
     * @return string
     */
    public function getLabelPathPrefix()
    {
        return $this->labelPathPrefix;
    }

    /**
     * setChildren
     *
     * @param  PropertySelection[] $children
     * @return null
     */
    public function setChildren(array $children)
    {
        $this->children = $children;
    }

    /**
     * getChildren
     *
     * @return PropertySelection[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * hasChildren
     *
     * @return boolean
     */
    public function hasChildren()
    {
        return $this->children !== array();
    }

    /**
     * Get one descendant. If there is less or more than one descendant, return null
     *
     * @return boolean
     */
    public function getOneDescendant()
    {
        if (count($this->children) !== 1) {
            return null;
        }

        $child = reset($this->children);

        if ($child->hasChildren()) {
            return $child->getOneDescendant();
        }

        return $child;
    }

    /**
     * find
     *
     * @param $path
     * @param $func
     * @return boolean
     */
    public function find($path, $func)
    {
        if ($this->getPath() === $path && $this->getFunc() === $func) {
            return $this;
        }

        foreach ($this->children as $child) {

            $found = $child->find($path, $func);

            if ($found) {
                return $found;
            }
        }
    }
}
