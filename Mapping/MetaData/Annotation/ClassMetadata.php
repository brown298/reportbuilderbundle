<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData\Annotation;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\AnnotationReaderInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\MetaData\AbstractClassMetadata;
use Doctrine\Common\Annotations\Reader;
use Exception;


/**
 * Class ClassMetadata
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
class ClassMetadata extends AbstractClassMetadata implements ClassMetadataInterface, AnnotationReaderInterface
{
    /**
     * @var string
     */
    protected $bundleAnnotations = 'Brown298\ReportBuilderBundle\Mapping\Annotation\%s';

    /**
     * @var FileCacheReader
     */
    protected $annotationReader;

    /**
     * getAnnotationClass
     *
     * @param string $class
     * @return string
     */
    public function getAnnotationClass($class)
    {
        // If the full class name pattern for annotation classes is set as a
        // property, allow the short class name to be used as a parameter
        if ($this->bundleAnnotations && strpos($class, '\\') === false) {
            $class = sprintf($this->bundleAnnotations, $class);
        }

        return $class;
    }

    /**
     * getAnnotationReader
     *
     * @throws \Exception
     * @return FileCacheReader
     */
    public function getAnnotationReader()
    {
        return $this->annotationReader;
    }

    /**
     * @param Reader $reader
     * @return mixed|void
     */
    public function setMetadataReader(Reader $reader)
    {
        $this->annotationReader = $reader;
    }

    /**
     * findMetadata
     *
     * @param string $annotationClass
     * @param null $targetClass
     * @return string
     */
    public function findMetadata($annotationClass, $targetClass)
    {
        $reflect         = $this->getReflectionClass($targetClass);
        $reader          = $this->getAnnotationReader();
        $annotationClass = $this->getAnnotationClass($annotationClass);
        $annotation      = $reader->getClassAnnotation($reflect, $annotationClass);

        return $annotation;
    }

}
