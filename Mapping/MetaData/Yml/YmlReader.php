<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData\Yml;
use Brown298\ReportBuilderBundle\Mapping\MetaData\AbstractConfigLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class YmlReader
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData\Yml
 */
class YmlReader extends AbstractConfigLoader
{
    /**
     * @var string
     */
    protected $annotationPathTemplate = '\Brown298\ReportBuilderBundle\Mapping\Annotation\%s';

    /**
     * @param ContainerInterface $container
     * @param array $configPathSearch
     */
    public function __construct(ContainerInterface $container, array $configPathSearch)
    {
        parent::__construct($container, $configPathSearch);
        $this->buildConfig();
    }

    /**
     * @param $targetClass
     * @param $metadataClass
     * @return null
     */
    public function findMetadataClass($targetClass, $metadataClass)
    {
        $result = null;

        $metadataClassArray = explode('\\', $metadataClass);
        $metadataClassArray = array_reverse($metadataClassArray);
        $metadataClassShort = $metadataClassArray[0];

        if (isset($this->config[$targetClass]) && isset($this->config[$targetClass]['contents']['Report'])) {
            $classMetadata = $this->config[$targetClass]['contents']['Report'];

            foreach ($classMetadata as $metaName => $metaValue) {

                if ($metaName == $metadataClassShort) {
                    if (!class_exists($metadataClass)) {
                        $fullPath = sprintf($this->annotationPathTemplate, $metaName);
                    } else {
                        $fullPath = $metadataClass;
                    }

                    if (is_array($metaValue)) {
                        $result = new $fullPath($metaValue);
                    } else {
                        $result = new $fullPath(array());
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $targetClass
     * @param $metadataClass
     * @param $property
     * @return null
     */
    public function findPropertyMetaData($targetClass, $metadataClass, $property)
    {
        $result = null;

        if (isset($this->config[$targetClass])
            && isset($this->config[$targetClass]['contents']['Report'])
            && isset($this->config[$targetClass]['contents']['Report']['Properties'])
        ) {
            $classMetadata = $this->config[$targetClass]['contents']['Report']['Properties'];
            foreach ($classMetadata as $propertyName=>$propertyMeta) {
                if ($propertyName == $property) {
                    foreach ($propertyMeta as $metaName=>$metaValue) {
                        if ($metaName == $metadataClass) {
                            $fullPath = sprintf($this->annotationPathTemplate, $metaName);
                            if (is_array($metaValue)) {
                                $result = new $fullPath($metaValue);
                            } else {
                                $result = new $fullPath(array());
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
}