<?php

namespace Brown298\ReportBuilderBundle\Mapping\MetaData\Yml;

use Brown298\ReportBuilderBundle\Filter\FilterTypeFactory;
use Brown298\ReportBuilderBundle\Filter\FilterTypeMap;
use Brown298\ReportBuilderBundle\Filter\RuntimeTypeFactory;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertyMetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\YmlReaderInterface;
use Brown298\ReportBuilderBundle\Mapping\MetaData\AbstractPropertyMetadata;

/**
 * Class PropertyMetadata
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData\Yml
 */
class PropertyMetadata extends AbstractPropertyMetadata implements YmlReaderInterface, PropertyMetadataInterface
{

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\Yml\YmlReader
     */
    protected $metadataReader;

    /**
     * @param $reader
     * @return mixed
     */
    public function setMetadataReader($reader)
    {
        $this->metadataReader = $reader;
    }

    /**
     * read the yml file to get the metadata if available
     *
     * @param $metadataClass
     * @param $targetClass
     * @return mixed|void
     */
    public function findMetadata($targetClass, $metadataClass, $property)
    {
        $metadata = $this->metadataReader->findPropertyMetaData($targetClass, $metadataClass, $property);
        return $metadata;
    }

    /**
     * isIgnored
     *
     * @return boolean
     */
    public function isIgnored()
    {
        $ignored = $this->findMetadata($this->getClass(), 'Ignored', $this->getName());
        if ($ignored) {

            if (!$ignored->hasFullPath()) {
                return true;
            }

            $ignoredEntity = $ignored->getEntity();
            $ignoredPaths = $ignored->getPaths();
            $class = $this->getTopClassMetadata()->getShortClass($this->getClass());
            $path = $this->getPath();

            if ($ignoredEntity === $class && in_array($path, $ignoredPaths)) {
                return true;
            }
        }
        return false;
    }

    /**
     * isSecured
     *
     * @return boolean
     */
    public function isSecured()
    {
        $secure = $this->findMetadata($this->getClass(), 'Secure', $this->getName());
        if ($secure) {
            if (!$this->security->checkSecuredAnnotation($secure)) {
                return true;
            }
        }

        return false;
    }

    /**
     * findRuntimeTypes
     */
    public function findRuntimeTypes()
    {
        $fieldMapping = $this->doctrineReader->findFieldMapping($this->getClass(), $this->getName());
        if ($fieldMapping) {
            $columnType = $fieldMapping['type'];
            $keys = FilterTypeMap::getKeysForColumn($columnType, 'runtime');

            $runtimeTypes = RuntimeTypeFactory::create($keys);
            if ($runtimeTypes) {
                return $runtimeTypes;
            }
        }

        return array();
    }

    /**
     * @return mixed
     */
    public function findFilterTypes()
    {
        // Get from filterable
        $filterable = $this->findMetadata($this->getClass(), 'Filter', $this->getName());
        if ($filterable) {

            $keys = $filterable->getTypes();
            if ($keys) {

                $filterTypes = FilterTypeFactory::create($keys);
                if ($filterTypes) {
                    return $filterTypes;
                }
            }

            $columnType = $filterable->getColumnType();
            if ($columnType) {

                $keys = FilterTypeMap::getKeysForColumn($columnType);
                $filterTypes = FilterTypeFactory::create($keys);
                if ($filterTypes) {
                    return $filterTypes;
                }
            }
        }

        // Parse from simple map
        $fieldMapping = $this->doctrineReader->findFieldMapping($this->getClass(), $this->getName());
        if ($fieldMapping) {
            $columnType = $fieldMapping['type'];
            $keys = FilterTypeMap::getKeysForColumn($columnType);

            $filterTypes = FilterTypeFactory::create($keys);
            if ($filterTypes) {
                return $filterTypes;
            }
        }

        return array();
    }

    /**
     * @param $translator
     * @return mixed
     */
    protected function getReportableLabel($translator)
    {
        $reportable = $this->findMetadata('Reportable', $this->getClass(), $this->name);
        if ($reportable && $reportable->hasLabel()) {
            $label = $reportable->getLabel();

            if ($translator) {
                $label = $translator->trans($label);
            }

            return $label;
        }
        return null;
    }
}