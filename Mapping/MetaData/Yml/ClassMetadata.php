<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData\Yml;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertyMetadata;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertySelection;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\YmlReaderInterface;
use Brown298\ReportBuilderBundle\Mapping\MetaData\AbstractClassMetadata;

/**
 * Class ClassMetadata
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData\Yml
 */
class ClassMetadata extends AbstractClassMetadata implements ClassMetadataInterface, YmlReaderInterface
{
    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\Yml\YmlReader
     */
    protected $metadataReader;


    /**
     * read the yml file to get the metadata if available
     *
     * @param $metadataClass
     * @param $targetClass
     * @return mixed|void
     */
    public function findMetadata($metadataClass, $targetClass)
    {
        $metadata = $this->metadataReader->findMetadataClass($targetClass, $metadataClass);
        return $metadata;
    }

    /**
     * @param $reader
     * @return mixed
     */
    public function setMetadataReader($reader)
    {
        $this->metadataReader = $reader;
    }

}