<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData;

use Brown298\ReportBuilderBundle\Common\Util\Inflector;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertyMetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;
use Brown298\ReportBuilderBundle\Mapping\Exception\CreationException;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface;

/**
 * Class AbstractPropertyMetadata
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
abstract class AbstractPropertyMetadata extends AbstractMetadata implements PropertyMetadataInterface
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var ClassMetadata
     */
    protected $classMetadata;

    /**
     * @var ClassMetadata|false
     */
    protected $associatedClassMetadata;

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\Annotation\Reportable
     */
    protected $reportable;

    /**
     * @var array
     */
    protected $filterTypes;

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\DoctrineReader
     */
    protected $doctrineReader;

    /**
     * @var null
     */
    protected $runtimeTypes = null;

    /**
     * @param ClassMetadataFactory $classMetadataFactory
     * @param ClassMetadataInterface $classMetadata
     * @param TreeMetadata $name
     * @param SecurityInterface $security
     * @param TreeMetadata $treeMetadata
     * @throws CreationException
     */
    public function __construct(
        ClassMetadataFactory   $classMetadataFactory,
        ClassMetadataInterface $classMetadata,
        $name,
        SecurityInterface      $security,
        TreeMetadata           $treeMetadata = null
    ) {
        if (!property_exists($classMetadata->getClass(), $name)) {
            $message = sprintf('Property %s::%s does not exist', $classMetadata->getClass(), $name);
            throw new CreationException($message);
        }
        $this->name                 = $name;
        $this->classMetadata        = $classMetadata;
        $this->doctrineReader       = $classMetadataFactory->getDoctrineReader();

        parent::__construct($classMetadataFactory, $security, $treeMetadata);
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * getClass
     *
     * @return string
     */
    public function getClass()
    {
        return $this->getClassMetadata()->getClass();
    }

    /**
     * Get depth
     *
     * @return integer
     */
    public function getDepth()
    {
        $pathPieces = $this->getPathPieces();
        $depth = count($pathPieces);

        return $depth;
    }


    /**
     * Recursively find path
     *
     * @return array
     */
    public function getPathPieces()
    {
        $pathPieces   = array();
        $pathPieces[] = $this->getName();

        $parent = $this->getClassMetadata()->getParentPropertyMetadata();

        if ($parent) {
            $more = $parent->getPathPieces();
            $pathPieces = array_merge($more, $pathPieces);
        }

        return $pathPieces;
    }

    /**
     * getLabelPathPrefix
     *
     * @return string
     */
    public function getLabelPathPrefix()
    {
        $pieces = $this->getLabelPathPieces();
        array_pop($pieces);
        $path = implode(' > ', $pieces);
        return $path;
    }

    /**
     * getLabelPath
     *
     * @return string
     */
    public function getLabelPath()
    {
        $pieces = $this->getLabelPathPieces();
        $path = implode(' > ', $pieces);
        return $path;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        $path = implode('.', $this->getPathPieces());
        return $path;
    }

    /**
     * @return mixed
     */
    public function isBase()
    {
        return ($this->getDepth() == 1) && !$this->hasAssociatedClass() ;
    }

    /**
     * getClassMeta
     *
     * @return ClassMetadata
     */
    public function getClassMetadata()
    {
        return $this->classMetadata;
    }

    /**
     * getLabelPathTruncated
     *
     * @return string
     */
    public function getLabelPathTruncated()
    {
        $pieces = $this->getLabelPathPieces();
        array_pop($pieces);
        array_shift($pieces);
        $path = implode(' > ', $pieces);
        return $path;
    }

    /**
     * Recursively find path
     *
     * @return array
     */
    public function getLabelPathPieces()
    {
        $pieces = array();

        $pieces[] = $this->getLabel();

        $classMeta = $this->getClassMetadata();
        $parent = $classMeta->getParentPropertyMetadata();

        // Recurse up the tree, add property labels
        if ($parent) {
            $more = $parent->getLabelPathPieces();
            $pieces = array_merge($more, $pieces);
        }

        // We've recursed to the top of the tree so add the original class'
        // label and stop recursing
        else {
            $label = $classMeta->getLabel();
            array_unshift($pieces, $label);
        }

        return $pieces;
    }

    /**
     * @throws CreationException
     */
    public function init()
    {
        if ($this->treeMetadata) {
            if (!$this->doctrineReader->isORMProperty($this->classMetadata->getClass(), $this->name)) {
                $message = sprintf('Property %s::%s is not a Doctrine\ORM\Mapping property', $this->classMetadata->getClass(), $this->name);
                throw new CreationException($message);
            }

            // check for ignored annotation
            if ($this->treeMetadata && $this->isIgnored()) {
                $message = sprintf('Property %s::%s is being ignored', $this->getClass(), $this->getName());

                throw new CreationException($message);
            }

            // check for secured annotation
            if ($this->treeMetadata && $this->isSecured()) {
                $message = sprintf('Property %s::%s is being restricted', $this->getClass(), $this->getName());

                throw new CreationException($message);
            }
        }
    }

    /**
     * findAssociatedClass
     *
     * @return string
     */
    protected function findAssociatedClass()
    {
        $targetClass = $this->classMetadata->getClass();
        return $this->doctrineReader->findAssociatedClass($targetClass, $this->name, $this->getClass());
    }

    /**
     * hasAssociatedClass
     *
     * @return bool
     */
    public function hasAssociatedClass()
    {
        return (bool)$this->getAssociatedClassMetadata();
    }

    /**
     * getAssociatedClassMeta
     *
     * @return ClassMetadataInterface|false
     */
    public function getAssociatedClassMetadata()
    {
        if (!is_null($this->associatedClassMetadata)) {
            return $this->associatedClassMetadata;
        }

        $classMeta = $this->findAssociatedClassMetadata();

        if ($classMeta) {
            $this->associatedClassMetadata = $classMeta;
        } else {
            $this->associatedClassMetadata = false;
        }

        return $this->associatedClassMetadata;
    }

    /**
     * findAssociatedClassMetadata
     *
     * @return ClassMetadataInterface
     */
    protected function findAssociatedClassMetadata()
    {
        $class = $this->findAssociatedClass();

        if (!$class) {
            return null;
        }

        try {
            $classMetadata = $this->classMetadataFactory->generate($class, $this->getTreeMetadata(), $this);
        } catch (CreationException $ex) {
            return null;
        }
        return $classMetadata;
    }

    /**
     * createSelection
     *
     * @return PropertySelection
     */
    public function createSelection()
    {
        $selection = new PropertySelection();
        $selection->setPath($this->getPath());
        $selection->setLabel($this->getLabel());
        $selection->setLabelPathPrefix($this->getLabelPathPrefix());

        return $selection;
    }

    /**
     * isAggregatable
     *
     * @return boolean
     */
    public function isAggregatable()
    {
        $type = $this->getDoctrineColumnType($this->getClass());
        $aggregatableTypes = array('text');
        $aggregatable = !in_array($type, $aggregatableTypes);

        return $aggregatable;
    }

    /**
     * getTopClassMeta
     *
     * @return ClassMetadata
     */
    public function getTopClassMetadata()
    {
        return $this->getClassMetadata()->getTopClassMetadata();
    }

    /**
     * getDoctrineColumnType
     *
     * @param $targetClass
     * @return string
     */
    public function getDoctrineColumnType($targetClass)
    {
        $fieldMapping = $this->doctrineReader->findFieldMapping($targetClass, $this->getName());
        if ($fieldMapping && !empty($fieldMapping[ 'type' ])) {
            return $fieldMapping[ 'type' ];
        }
        return null;
    }


    /**
     * isChildFreeParentInTree
     *
     * @return string
     */
    public function isChildFreeParentInTree()
    {
        // Not parent
        if (!$this->hasAssociatedClass()) {
            return false;
        }

        $classMeta         = $this->getAssociatedClassMetadata();
        $propertyMetadatas = $classMeta->getPropertyMetadatasForTree();
        $count             = count($propertyMetadatas);

        return ($count === 0);
    }


    /**
     * findLabel
     *
     * @return string
     */
    protected function findLabel()
    {
        $translator = null;
        $treeMetadata = $this->getTreeMetadata();
        if ($treeMetadata) {
            $translator = $treeMetadata->getTranslator();
        }

        // Get from reportable
        $reportable = $this->getReportableLabel($translator);
        if ($reportable) {
            return $reportable;
        }

        // Parse label from object properties
        $label = $this->getName();
        $label = Inflector::labelize($label);

        if ($translator) {
            $label = $translator->trans($label);
        }

        return $label;
    }


    /**
     * getFilterTypes
     *
     * @return array
     */
    public function getFilterTypes()
    {
        if (is_null($this->filterTypes)) {
            $this->filterTypes = $this->findFilterTypes();
        }

        return $this->filterTypes;
    }

    /**
     * getRuntimeTypes
     *
     * @return array
     */
    public function getRuntimeTypes()
    {
        if (is_null($this->runtimeTypes)) {
            $this->runtimeTypes = $this->findRuntimeTypes();
        }

        return $this->runtimeTypes;
    }

    /**
     * @return null
     */
    public function findJoinType()
    {
        $joinType = $this->findMetadata($this->getClass(), 'JoinType', $this->getName());
        if ($joinType) {
            return $joinType->getJoinType();
        };

        return null;
    }

    /**
     * @param $translator
     * @return mixed
     */
    abstract protected function getReportableLabel($translator);

    /**
     * @return mixed
     */
    abstract public function findFilterTypes();
}