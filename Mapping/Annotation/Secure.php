<?php
namespace Brown298\ReportBuilderBundle\Mapping\Annotation;

/**
 * Secure
 *
 * provides role based security
 *
 * @Annotation
 * @Target({"CLASS","PROPERTY"})
 */
class Secure
{
    /**
     * Roles
     *
     * @var array
     */
    private $roles;

    /**
     * Constructor
     *
     * @param array $values
     * @throws \InvalidArgumentException
     * @return \Brown298\ReportBuilderBundle\Mapping\Annotation\Secure
     */
    public function __construct($values)
    {
        if (empty($values['roles'])) {
            throw new \InvalidArgumentException('You must define a "roles" attribute for each Secure annotation.');
        }

        $roles = $values['roles'];
        $roles = explode(',', $roles);
        $roles = array_map('trim', $roles);

        $this->roles = $roles;
    }

    /**
     * getRoles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }
}