<?php
namespace Brown298\ReportBuilderBundle\Mapping\Annotation;

use InvalidArgumentException;

/**
 * Ignored
 *
 * @Annotation
 * @Target({"CLASS", "PROPERTY"})
 */
class Ignored
{
    /**
     * @var string
     */
    private $entity;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string[]
     */
    private $paths;

    /**
     * Constructor
     *
     * @param $values
     * @throws
     * @internal param string $entity
     * @internal param string $path
     * @return \Brown298\ReportBuilderBundle\Mapping\Annotation\Ignored
     */
    public function __construct($values)
    {
        if (!empty($values['entity']) || !empty($values['path'])) {

            if (empty($values['entity']) || empty($values['path'])) {
                throw new InvalidArgumentException('The "entity" and "path" attributes must be provided  together or not at all');
            }

            $this->entity = $values['entity'];
            $this->path   = $values['path'];
            $this->paths  = $this->parsePaths($values['path']);
        }
    }

    /**
     * getEntity
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * getPath
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * getPaths
     *
     * @return string[]
     */
    public function getPaths()
    {
        return $this->paths;
    }

    /**
     * hasFullPath
     *
     * @return bool
     */
    public function hasFullPath()
    {
        return !empty($this->entity) && !empty($this->path);
    }

    /**
     * parsePaths
     *
     * @param  string $path
     * @return string[]
     */
    protected function parsePaths($path)
    {
        $path = str_replace('*', ' ', $path);
        $paths = explode(',', $path);
        $paths = array_map('trim', $paths);
        $paths = array_filter($paths);

        return $paths;
    }
}