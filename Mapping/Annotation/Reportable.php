<?php
namespace Brown298\ReportBuilderBundle\Mapping\Annotation;

/**
 * Reportable
 *
 * @Annotation
 * @Target({"CLASS", "PROPERTY"})
 */
class Reportable
{
    /**
     * Label
     *
     * @var string
     */
    private $label;

    /**
     * Constructor
     *
     * @param array $values
     * @return \Brown298\ReportBuilderBundle\Mapping\Annotation\Reportable
     */
    public function __construct($values)
    {
        if (!empty($values['label'])) {
            $this->label = $values['label'];
        }
    }

    /**
     * getLabel
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * hasLabel
     *
     * @return bool
     */
    public function hasLabel()
    {
        return (bool) $this->label;
    }
}