<?php
namespace Brown298\ReportBuilderBundle\Mapping\Annotation;

use InvalidArgumentException;

/**
 * Class JoinType
 * @package Brown298\ReportBuilderBundle\Mapping\Annotation
 * @Annotation
 * @Target({"CLASS", "PROPERTY"})
 */
class JoinType
{

    /**
     * @var string
     */
    protected $joinType;

    /**
     * Constructor
     *
     * @param $values
     * @return \Brown298\ReportBuilderBundle\Mapping\Annotation\JoinType
     */
    public function __construct(array $values = null)
    {
        if(!isset($values['type'])) {
            throw new InvalidArgumentException('You must specify a join type');
        }

        $this->joinType = $values['type'];
    }

    /**
     * @return string
     */
    public function getJoinType()
    {
        return $this->joinType;
    }
}