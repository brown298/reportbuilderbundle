<?php
namespace Brown298\ReportBuilderBundle\Mapping\Annotation;

/**
 * Filter
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Filter
{
    /**
     * columnType
     *
     * @var string
     */
    private $columnType;

    /**
     * Types
     *
     * @var array
     */
    private $types;

    /**
     * Constructor
     *
     * @param array $args
     * @return \Brown298\ReportBuilderBundle\Mapping\Annotation\Filter
     */
    public function __construct($args)
    {
        if (isset($args['types'])) {
            $this->types = $args['types'];
        }
        if (isset($args['columnType'])) {
            $this->columnType = $args['columnType'];
        }
    }

    /**
     * getColumnType
     *
     * @return string
     */
    public function getColumnType()
    {
        return $this->columnType;
    }

    /**
     * getTypes
     *
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * hasTypes
     *
     * @return bool
     */
    public function hasTypes()
    {
        return !empty($this->types);
    }
}