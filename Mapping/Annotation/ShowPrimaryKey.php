<?php
namespace Brown298\ReportBuilderBundle\Mapping\Annotation;

/**
 * ShowPrimaryKey
 *
 * @Annotation
 * @Target("CLASS")
 */
class ShowPrimaryKey
{

    /**
     * Constructor
     *
     * @param array $args
     * @return \Brown298\ReportBuilderBundle\Mapping\Annotation\ShowPrimaryKey
     */
    public function __construct($args)
    {

    }

    public function getValue()
    {
        return true;
    }
}