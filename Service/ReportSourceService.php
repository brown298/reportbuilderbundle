<?php
namespace Brown298\ReportBuilderBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;

/**
 * Provides static report objects with a consistent set of resources
 *
 */
class ReportSourceService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var FormFactory $formFactory
     */
    private $formFactory;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param FormFactory $formFactory
     */
    public function __construct(
        EntityManager $em, FormFactory $formFactory
    ) {
        $this->em = $em;
        $this->formFactory = $formFactory;
    }

    /**
     * getEntityManager
     *
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * getFormFactory
     *
     * @return FormFactory
     */
    public function getFormFactory()
    {
        return $this->formFactory;
    }
}