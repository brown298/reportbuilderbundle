<?php
namespace Brown298\ReportBuilderBundle\Service\Security;

use Brown298\ReportBuilderBundle\Mapping\Annotation\Secure;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportContainerInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class NullSecurityService
 *
 * @package Brown298\ReportBuilderBundle\Service\Security
 */
class NullSecurityService implements SecurityInterface
{

    /**
     * Returns true if the passed class is not being restricted for the current
     * user. Reads the Secure annotation on the entity.
     *
     * @param string $class
     *
     * @return bool
     */
    public function isViewingAllowed($class)
    {
        return true;
    }

    /**
     * Throws an exception if the current user is not allowed to perform the
     * passed action on the current report's type (system, built, others'
     * built).
     *
     * @param  string $action
     * @param  string $modifier
     *
     * @throws AccessDeniedException
     * @return null
     */
    public function checkPermissions($action, $modifier = null)
    {
        return;
    }

    /**
     * Returns false if the current user is not allowed to perform the passed
     * action on the current report's type (system, built, others' built).
     *
     * @param  string $action
     * @param  string $modifier
     *
     * @return boolean
     */
    public function hasPermissions($action, $modifier = null)
    {
       return true;
    }

    /**
     * getName
     *
     * gets the name of the current service
     *
     * @return string
     */
    public function getName()
    {
        return 'brown298.report_builder.security.null';
    }

    /**
     * setReportContainer
     *
     * @param ReportContainerInterface $reportContainer
     *
     * @return null
     */
    public function setReportContainer(ReportContainerInterface $reportContainer = null)
    {
        return;
    }

    /**
     * Throws an exception if the current report contains classes that the
     * current user is not allowed to view.
     *
     * @throws AccessDeniedException
     * @return null
     */
    public function checkReportClassPermissions()
    {
        return null;
    }

    /**
     * getCanEditOthers
     *
     * determines if current user can edit other user's reports
     * @return bool
     */
    public function getCanEditOthers()
    {
        return false;
    }

    /**
     * checkSecuredAnnotation
     *
     * checks the user against a secure annotation
     *
     * @param Secure $secure
     *
     * @return boolean
     */
    public function checkSecuredAnnotation(Secure $secure)
    {
        return true;
    }
}