<?php
namespace Brown298\ReportBuilderBundle\Service\Security;

use Brown298\ReportBuilderBundle\Mapping\Annotation\Secure;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportContainerInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;
use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class RoleSecurityService
 *
 * an example of a security service based on role permissions
 *
 * @package Brown298\ReportBuilderBundle\Service
 */
class RoleSecurityService implements SecurityInterface
{
    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var TreeMetadata
     */
    protected $treeMetadata;

    /**
     * @var ReportContainerInterface
     */
    protected $reportContainer;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     *
     * @param SecurityContext $securityContext
     * @param TreeMetadata $treeMetadata
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        SecurityContext $securityContext,
        TreeMetadata    $treeMetadata,
        LoggerInterface $logger = null
    ) {
        $this->securityContext = $securityContext;
        $this->treeMetadata    = $treeMetadata;
        $this->logger          = $logger;
    }

    /**
     * @param $message
     * @param string $level
     */
    protected function Log($message, $level = 'INFO')
    {
        if ($this->logger != null) {
            $this->logger->log($level, $message);
        }
    }

    /**
     * getSecurityContext
     *
     * @return SecurityContext
     */
    public function getSecurityContext()
    {
        return $this->securityContext;
    }

    /**
     * getTreeMetadata
     *
     * @return TreeMetadata
     */
    public function getTreeMetadata()
    {
        return $this->treeMetadata;
    }

    /**
     * setReportContainer
     *
     * @param ReportContainerInterface $reportContainer
     * @return null
     */
    public function setReportContainer(ReportContainerInterface $reportContainer = null)
    {
        $this->reportContainer = $reportContainer;
    }

    /**
     * Throws an exception if the current report contains classes that the
     * current user is not allowed to view.
     *
     * @throws AccessDeniedException
     * @return null
     */
    public function checkReportClassPermissions()
    {
        if (!$this->areReportClassesPermissionsValid()) {
            throw new AccessDeniedException;
        }
    }

    /**
     * Returns true if the current user is allowed to view all classes included
     * in the report.
     *
     * @return bool
     */
    public function areReportClassesPermissionsValid()
    {
        $this->assertReportContainer();

        $classes = $this->reportContainer->getIncludedClasses();

        foreach ($classes as $class) {
            if (!$this->isViewingAllowed($class)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns true if the passed class is not being restricted for the current
     * user. Reads the Secure annotation on the entity.
     *
     * @param string $class
     * @return bool
     */
    public function isViewingAllowed($class)
    {
        $roles = $this->treeMetadata->getRoles($class);

        foreach ($roles as $role) {
            $this->log($class . '::' . $role . ':' . ($this->getSecurityContext()->isGranted($role)?'true':'false'));
            if (!$this->getSecurityContext()->isGranted($role)) {
                $this->log('permission denied:' . $role . ' ' . $class);
                return false;
            }
        }

        return true;
    }


    /**
     * Throws an exception if the current user is not allowed to perform the
     * passed action on the current report's type (system, built, others'
     * built).
     *
     * @param  string $action
     * @param  string $modifier
     * @throws AccessDeniedException
     * @return null
     */
    public function checkPermissions($action, $modifier = null)
    {
        if (!$this->hasPermissions($action, $modifier)) {
            throw new AccessDeniedException($this->getRoleName($action, $modifier));
        }
    }

    /**
     * Returns false if the current user is not allowed to perform the passed
     * action on the current report's type (system, built, others' built).
     *
     * @param  string $action
     * @param null $modifier
     * @param  string $modifier
     * @return boolean
     */
    public function hasPermissions($action, $modifier = null)
    {
        $securityContext = $this->getSecurityContext();
        $permission      = $this->getRoleName($action, $modifier);
        $allowed         = $securityContext->isGranted($permission);

        if (!$allowed) {
            $this->log(sprintf('Access Denied for user %s accessing %s', $securityContext->getToken()->getUsername(), $permission));
        }

        return $allowed;
    }

    /**
     * @param $action
     * @param null $modifier
     * @return string
     */
    protected function getRoleName($action, $modifier = null)
    {
        $securityContext = $this->getSecurityContext();
        $user            = $securityContext->getToken()->getUser();

        if (!$modifier && $this->reportContainer != null) {
            $permission = 'ROLE_%s_REPORT_%s';
            $modifier = $this->reportContainer->getPermissionModifier($user);
            $permission = sprintf($permission, $modifier, $action);
        } else {

            $permission = 'ROLE_REPORT_%s';
            $permission = sprintf($permission, $action);
        }

        return $permission;
    }

    /**
     * getName
     *
     * @throws \Exception
     * @return null
     */
    private function assertReportContainer()
    {
        if (!$this->reportContainer) {
            throw new \Exception('Report container not set');
        }
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return 'brown298.report_builder.security.role';
    }

    /**
     * Checks if the attributes are granted against the current token.
     *
     * @param  mixed   $attributes
     * @param  mixed   $object
     * @return boolean
     */
    public function isGranted($attributes, $object = null)
    {
        return $this->securityContext->isGranted($attributes, $object);
    }

    /**
     * getCanEditOthers
     *
     * determines if current user can edit other user's reports
     * @return bool
     */
    public function getCanEditOthers()
    {
        return $this->isGranted('ROLE_REPORT_UPDATE_OTHERS');
    }

    /**
     * checkSecuredAnnotation
     *
     * checks the user against a secure annotation
     *
     * @param Secure $secure
     *
     * @return boolean
     */
    public function checkSecuredAnnotation(Secure $secure)
    {
        $roles           = $secure->getRoles();
        $securityContext = $this->getSecurityContext();
        $allowed         = false;
        foreach ($roles as $role) {
            if ($securityContext->isGranted($role)) {
                $allowed = true;
            }
        }

        if (!$allowed) {
            $this->log(sprintf('Access Denied for user %s', $securityContext->getToken()->getUsername()));
        }

        return $allowed;
    }
}













