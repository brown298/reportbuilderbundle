<?php
namespace Brown298\ReportBuilderBundle\Service\ReportContainer;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Brown298\ReportBuilderBundle\Entity\BuiltReport;
use Brown298\ReportBuilderBundle\Mapping\MetaData\ClassMetadata;

/**
 * Class ReportQueryBuilder
 *
 * @package Brown298\ReportBuilderBundle\Service\ReportContainer
 * @author John Brown <john.brown@partnerweekly.com>
 */
class ReportQueryBuilder extends QueryBuilder
{
    /**
     * DQL alias naming pattern
     */
    const ALIAS = 'e%d';

    /**
     * DQL count property alias
     *
     * Cannot be set to "count" because of naming restrictions.
     */
    const COUNT_ALIAS = 'count_';

    /**
     * maximum safe size of a query among any of its tables
     *
     * Meant to prevent large table scans
     */
    const MAX_ROW_COUNT = 6000000;

    /**
     * @var \Brown298\ReportBuilderBundle\Entity\Report
     */
    protected $report;

    /**
     * @var ClassMetadata
     */
    protected $classMetadata;

    /**
     * @var string[]
     */
    protected $selects = array();

    /**
     * @var string[]
     */
    protected $groupBys = array();

    /**
     * @var string[string] Class names by path
     */
    protected $joinClasses = array();

    /**
     * @var string
     */
    protected $mainAlias;

    /**
     * @var integer
     */
    protected $aliasIdx = 0;

    /**
     * @var SelectField[]
     */
    protected $selectFields;

    /**
     * Constructor
     *
     * @param  EntityManager   $entityManager
     * @param  BuiltReport     $report
     * @param  ClassMetadataInterface   $classMeta
     */
    public function __construct( EntityManager $entityManager, BuiltReport $report, ClassMetadataInterface $classMeta )
    {
        parent::__construct($entityManager);

        $this->report = $report;
        $this->classMetadata = $classMeta;

        $this
            ->applyFrom()
            ->applySelect()
            ->applyWhere()
            ->applyGroupBy()
            ->applyHaving()
            ->applyOrder()
        ;
    }

    /**
     * applyFrom
     *
     * @return ReportQueryBuilder This instance
     */
    protected function applyFrom()
    {
        $entity = $this->report->getEntity();
        $alias = $this->createAlias();

        $this->from($entity, $alias);
        $this->registerMainAlias($alias);

        return $this;
    }

    /**
     * applySelect
     *
     * @return ReportQueryBuilder This instance
     */
    protected function applySelect()
    {
        $selectFields = $this->getSelectFields();

        foreach ($selectFields as $selectField) {

            $property = $selectField->property;
            $alias    = $selectField->alias;

            $select = sprintf('%s AS %s', $property, $alias);
            $this->andSelect($select);
        }

        return $this;
    }

    /**
     * applyGroupBy
     *
     * @return ReportQueryBuilder This instance
     */
    protected function applyGroupBy()
    {
        if ($this->report->hasCountFields()) {
            $selectFields = $this->getSelectFields();

            foreach ($selectFields as $selectField) {

                $reportField = $selectField->reportField;
                if ($reportField->getFunction()) {
                    continue;
                }

                $property = $selectField->property;
                $this->andGroupBy($property);
            }
        }

        return $this;
    }

    /**
     * applyWhere
     *
     * @return ReportQueryBuilder This instance
     */
    protected function applyWhere()
    {
        $filters = $this->report->getFilters();

        if (empty($filters) || ($filters instanceof PersistentCollection && $filters->count() == 0)) {
            $filters = $this->createDefaultFilters();
        }

        foreach ($filters as $filter) {
            if ($filter->isCountFilter()) {
                continue;
            }

            // Sometimes the table display on the filter step view will use
            // invalid filters if the form is currently using request data and
            // showing errors
            if (!$filter->isValid()) {
                continue;
            }

            $path = $filter->getPath();
            $property = $this->getAliasedProperty($path);

            $filterType = $filter->getFilterTypeObject();
            $filterType->applyFilter($this, $filter, $property);
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function createDefaultFilters()
    {
        return $this->classMetadata->findDefaultFilter();
    }

    /**
     * applyHaving
     *
     * @return ReportQueryBuilder This instance
     */
    protected function applyHaving()
    {
        $filters = $this->report->getCountFilters();

        foreach ($filters as $filter) {
            $property = self::COUNT_ALIAS;

            $filterType = $filter->getFilterTypeObject();
            $filterType->applyFilter($this, $filter, $property, true);
        }

        return $this;
    }

    /**
     * applyOrder
     *
     * @return ReportQueryBuilder This instance
     */
    protected function applyOrder()
    {
        $orders = $this->report->getOrders();

        foreach ($orders as $order) {

            if ($order->isCountOrder()) {
                $property = self::COUNT_ALIAS;
            } else {
                $path = $order->getPath();
                $property = $this->getAliasedProperty($path);
            }

            $direction = $order->isAscending() ? 'ASC' : 'DESC';

            $this->addOrderBy($property, $direction);
        }

        return $this;
    }

    /**
     * getAliasedProperty
     *
     * @param string $path
     * @param string $separator
     * @return string
     */
    protected function getAliasedProperty($path, $separator = '.')
    {
        $alias = $this->getClassAliasFromPropertyPath($path);
        $property = $this->getPropertyFromPath($path);
        $aliasedProperty = $alias.$separator.$property;

        return $aliasedProperty;
    }

    /**
     * getAliasedIdentifierProperty
     *
     * @param string $separator
     * @return string
     */
    protected function getAliasedIdentifierProperty($separator = '.')
    {
        $alias            = $this->getMainAlias();
        $propertyMetadata = $this->classMetadata->getIdentifierPropertyMetadata();
        $property         = $propertyMetadata;
        $aliasedProperty  = $alias.$separator.$property;

        return $aliasedProperty;
    }

    /**
     * Get multi dimensional array of rows
     *
     * @return string[][]
     */
    public function getData()
    {
        $query = $this->getQuery();
        $data = $query->getResult(Query::HYDRATE_SCALAR);
        $selectFields = $this->getSelectFields();
        $rows = array();

        foreach ($data as $values) {
            $row = array();

            foreach ($selectFields as $selectField) {
                $alias        = $selectField->alias;
                $propertyMeta = $selectField->propertyMetadata;
                $value        = $values[$alias];
                $value        = $this->getValueString($value, $propertyMeta);

                $row[] = $value;
            }

            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * Returns a string representation of a value returned from a Doctrine query
     *
     * @param  mixed                                      $value
     * @param  \Brown298\ReportBuilderBundle\Mapping\MetaData\PropertyMetadata $propertyMetadata
     *
     * @return string
     */
    public function getValueString($value, $propertyMetadata)
    {
        if ($propertyMetadata) {

            $type = $propertyMetadata->getDoctrineColumnType($propertyMetadata->getClass());

            if ($type === 'boolean') {
                return $value ? 'Yes' : 'No';
            }
        }

        if (is_object($value)) {

            if (method_exists($value, '__isString')) {
                return $value->__isString();
            }

            if (get_class($value) === 'DateTime') {
                $date = $value->format('n/j/Y H:i:s');

                // Date without time
                if (substr($date, -9) === ' 00:00:00') {
                    $date = substr($date, 0, -9);
                }

                return $date;
            }
        }

        return (string) $value;
    }

    /**
     * getPropertyFromPath
     *
     * @param  string $path
     * @return string
     */
    protected function getPropertyFromPath($path)
    {
        $pos = strrpos($path, '.');

        if ($pos === false) {
            return $path;
        }

        return substr($path, $pos + 1);
    }

    /**
     * registerMainAlias
     *
     * @param  string $alias
     * @return null
     */
    protected function registerMainAlias($alias)
    {
        $this->mainAlias = $alias;
    }

    /**
     * getMainAlias
     *
     * @throws Exception
     * @return string
     */
    public function getMainAlias()
    {
        if (!$this->mainAlias) {
            $message = 'Main alias not registered yet.'
                .' Run applyFrom method to register.';
            throw new Exception($message);
        }

        return $this->mainAlias;
    }

    /**
     * createAlias
     *
     * @return string
     */
    protected function createAlias()
    {
        $alias = sprintf(self::ALIAS, $this->aliasIdx);
        $this->aliasIdx++;

        return $alias;
    }

    /**
     * andSelect
     *
     * @param  mixed $selects
     * @return BuiltReportQueryBuilder This instance
     */
    public function andSelect($selects)
    {
        if (!is_array($selects)) {
            $selects = func_get_args();
        }

        $this->selects = array_merge($this->selects, $selects);
        $this->selects = array_unique($this->selects);

        return $this->select($this->selects);
    }

    /**
     * andGroupBy
     *
     * @param  mixed $groupBys
     * @return BuiltReportQueryBuilder This instance
     */
    public function andGroupBy($groupBys)
    {
        if (!is_array($groupBys)) {
            $groupBys = func_get_args();
        }

        $this->groupBys = array_merge($this->groupBys, $groupBys);
        $this->groupBys = array_unique($this->groupBys);

        return $this->groupBy(implode(', ', $this->groupBys));
    }

    /**
     * Returns the class alias of a property path
     *
     * @param  string $path
     * @return string       DQL alias
     */
    protected function getClassAliasFromPropertyPath($path)
    {
        $pos = strrpos($path, '.');

        // Property is on main entity
        if ($pos === false) {
            return $this->getMainAlias();
        }

        $classPath = substr($path, 0, $pos);
        $alias = $this->joinClass($classPath);

        return $alias;
    }

    /**
     * joinClass
     *
     * @param  string  $path
     * @param  boolean $skipChecks if the checks for ignored etc should be considered
     *
     * @throws \Exception
     * @return string alias
     */
    public function joinClass($path, $skipChecks = false)
    {
        $joinType = null;
        if (isset($this->joinClasses[$path])) {
            return $this->joinClasses[$path];
        }

        if (!$skipChecks) {

            if (!$this->classMetadata->isValidPath($path)) {
                $message = sprintf('Invalid path %s', $path);
                throw new Exception($message);
            }
            /** @var \Brown298\ReportBuilderBundle\Mapping\MetaData\Yml\PropertyMetadata $propertyMeta */
            $propertyMeta = $this->classMetadata->getPropertyMetadataByPath($path);

            $joinType = $propertyMeta->findJoinType();

            if (!$propertyMeta->hasAssociatedClass()) {
                $message = sprintf('Not associated with a class: %s', $path);
                throw new Exception($message);
            }
        }

        // load class meta if available and none is set on the property
        if ($joinType == null) {
            $joinType = $this->classMetadata->findJoinType();
        }

        // Figure out parent alias and property name
        $pos = strrpos($path, '.');
        if ($pos !== false) {
            $parentPath = substr($path, 0, $pos);
            $parentAlias = $this->joinClass($parentPath, $skipChecks);
        } else {
            $parentAlias = $this->getMainAlias();
        }

        // Get property name

        $property = $this->getPropertyFromPath($path);

        // Create new alias
        $alias = $this->createAlias();

        // Add join to query builder
        $join = sprintf('%s.%s', $parentAlias, $property);

        if ($joinType == null) {
            $this->leftJoin($join, $alias);
        } else {
            switch ($joinType) {
                case 'InnerJoin':
                    $this->innerJoin($join, $alias);
                    break;
                default:
                    $this->leftJoin($join, $alias);
                    break;
            }
        }


        $this->joinClasses[$path] = $alias;

        return $alias;
    }

    /**
     * getSelectFields
     *
     * @return SelectField[]
     */
    public function getSelectFields()
    {
        if ($this->selectFields) {
            return $this->selectFields;
        }

        $fields = $this->report->getFields();
        $selectFields = array();

        foreach ($fields as $field) {

            $selectField = new SelectField;
            $selectField->reportField = $field;

            if ($field->isCountField()) {
                $propertyName = $this->getAliasedIdentifierProperty();
                $selectField->property = sprintf('COUNT(%s)', $propertyName);
                $selectField->alias = self::COUNT_ALIAS;
            } else {
                $path = $field->getPath();
                if ($field->getFunction() && stripos($field->getFunction(), '%s') <= 0) {
                    $selectField->property = sprintf('%s(%s)', $field->getFunction(), $this->getAliasedProperty($path));
                } else if($field->getFunction()) {
                    $selectField->property = sprintf($field->getFunction(), $this->getAliasedProperty($path));
                } else {
                    $selectField->property = $this->getAliasedProperty($path);
                }
                $selectField->alias = $this->getAliasedProperty($path, '_');
                $selectField->propertyMetadata = $this->classMetadata->getPropertyMetadataByPath($path);
            }

            $selectFields[] = $selectField;

            $this->selectFields = $selectFields;
        }

        return $this->selectFields;
    }

    /**
     * @return ClassMetadata
     */
    public function getClassMetadata()
    {
        return $this->classMetadata;
    }

    /**
     * @return Query
     * @throws Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getQuery()
    {
        $query = parent::getQuery();

        $sql = $query->getSQL();

        if (!preg_match('/limit/i', $sql)) {
            $sql .= ' Limit 100';
        }

        $stmt = $query->getEntityManager()->getConnection()->prepare('explain ' . $sql);
        $params = array();

        /** @var \Doctrine\ORM\Query\Parameter $param */
        foreach ($query->getParameters() as $param) {
            $value = $param->getValue();
            if ($value instanceof \DateTime) $value = $value->format(\DateTime::ISO8601);
            $params[] = $value;
        }

        $explainQueryResult = $stmt->execute($params);
        if ($explainQueryResult) {
            foreach ($stmt->fetchAll() as $tableValue) {
                // protect against large table scans
                if ($tableValue['rows'] > self::MAX_ROW_COUNT) {

                    throw new Exception('A query was created that resulted in too many rows being selected for table ' . $tableValue['table']
                        . ".\n\n" . 'Please add filters to reduce the size' . "\n\n" . $query->getSQL());
                }
            }
        }

        $query->useQueryCache(true);

        return $query;
    }
} 