<?php
namespace Brown298\ReportBuilderBundle\Service\ReportContainer;

use Brown298\ReportBuilderBundle\Entity\Report;

/**
 * Class AbstractReportContainer
 *
 * @package Brown298\ReportBuilderBundle\Service\ReportContainer
 */
abstract class AbstractReportContainer
{
    /**
     * @var Report
     */
    protected $report;

    /**
     * Constructor
     *
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    /**
     * getPermissionModifier
     *
     * @param int $userId
     * @return string
     */
    public function getPermissionModifier($userId)
    {
        if ($this->report->isSystem()) {
            return 'SYSTEM';
        }

        if ($this->report->isBuilt()) {
            if ($this->report->getCreatedBy() !== $userId) {
                return 'OTHERS_BUILT';
            } else {
                return 'BUILT';
            }
        }

        return 'STATIC';
    }

    /**
     * Get report
     *
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Get parameter form
     *
     * @return null
     */
    public function getParameterForm()
    {
        return null;
    }

    /**
     * getKey
     *
     * @return string
     */
    public function getKey()
    {
        return 'report'.$this->report->getId();
    }
} 