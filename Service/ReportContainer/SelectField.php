<?php
namespace Brown298\ReportBuilderBundle\Service\ReportContainer;

/**
 * Class SelectField
 *
 * @package Brown298\ReportBuilderBundle\Service\ReportContainer
 */
class SelectField 
{
    /**
     * @var \Brown298\ReportBuilderBundle\Entity\Field
     */
    public $field;

    /**
     * @var string "e1.property" or "COUNT(e0.id)"
     */
    public $property;

    /**
     * @var string "e1_property" or "count_"
     */
    public $alias;

    /**
     * @var Brown298\ReportBuilderBundle\Mapping\MetaData\PropertyMetadata
     */
    public $propertyMetadata;
}