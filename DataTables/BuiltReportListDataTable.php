<?php
namespace Brown298\ReportBuilderBundle\DataTables;

use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class BuiltReportListDataTable
 * @package Brown298\ReportBuilderBundle\DataTables
 */
class BuiltReportListDataTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface
{
    /**
     * @var array
     */
    protected $columns = array(
        'r.name'      => 'Name',
        'r.published' => 'Status',
        'r.shared'    => 'Shared',
        'r.id'        => 'Actions',
    );

    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * @var bool
     */
    protected $includeDeleted = false;

    /**
     * @var bool
     */
    protected $includeShared = true;

    /**
     * @var null
     */
    protected $userName = null;

    /**
     * @param bool $includeShared
     * @param bool $includeDeleted
     * @param string $userName
     */
    public function __construct($includeShared, $userName = null, $includeDeleted = false)
    {
        $this->includeDeleted = $includeDeleted;
        $this->includeShared  = $includeShared;
        $this->userName       = $userName;

        if (!$this->includeShared) {
            unset($this->columns['r.shared']);
        }
    }

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $reportRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('Brown298\ReportBuilderBundle\Entity\Report');
        $qb = $reportRepository->getOwnedForIndexQb($this->includeDeleted);

        if ($this->userName != null) {
            $qb->andWhere('(r.createdBy = :createdBy or r.updatedBy = :updatedBy)')
                ->setParameter('createdBy', $this->userName)
                ->setParameter('updatedBy', $this->userName);
        }

        return $qb;
    }

    /**
     * getDataFormatter
     *
     * @return \Closure
     */
    public function getDataFormatter()
    {
        $renderer      = $this->container->get('templating');
        $includeShared = $this->includeShared;

        return function($data) use ($renderer, $includeShared) {
            $count   = 0;
            $results = array();

            foreach ($data as $row) {
                $results[$count][] = $row->getName();
                $results[$count][] = $row->getCompleteness();
                if ($includeShared) {
                    $results[$count][] = $renderer->render('Brown298ReportBuilderBundle:Index:BuiltReportList/shared.html.twig', array('report' => $row));
                }
                $results[$count][] = $renderer->render('Brown298ReportBuilderBundle:Index:BuiltReportList/actions.html.twig', array('report' => $row));
                $count += 1;
            }

            return $results;
        };
    }
}
