<?php
namespace Brown298\ReportBuilderBundle\DataTables;

use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class BuiltReportListDataTable
 * @package Brown298\ReportBuilderBundle\DataTables
 */
class SharedReportListDataTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface
{
    /**
     * @var array
     */
    protected $columns = array(
        'r.name'      => 'Name',
        'r.published' => 'Status',
        'r.shared'    => 'Shared',
        'r.id'        => 'Actions',
    );

    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * @var bool
     */
    protected $includeDeleted = false;

    /**
     * @var string
     */
    protected $userName;

    /**
     * @var bool
     */
    protected $canEditOthers;

    /**
     * @param null $userName
     * @param bool $canEditOthers
     * @param bool $includeDeleted
     */
    public function __construct($userName  = null, $canEditOthers = false, $includeDeleted = false)
    {
        $this->includeDeleted = $includeDeleted;
        $this->userName       = $userName;
        $this->canEditOthers  = $canEditOthers;
    }

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $reportRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('Brown298\ReportBuilderBundle\Entity\Report');
        $qb = $reportRepository->getSharedForIndexQb($this->includeDeleted, $this->canEditOthers);

        if ($this->userName != null) {
            $qb->andWhere('(r.createdBy != :createdBy and r.updatedBy != :updatedBy)')
                ->setParameter('createdBy', $this->userName)
                ->setParameter('updatedBy', $this->userName);
        }

        return $qb;
    }

    /**
     * getDataFormatter
     *
     * @return \Closure
     */
    public function getDataFormatter()
    {
        $renderer = $this->container->get('templating');
        return function($data) use ($renderer) {
            $count   = 0;
            $results = array();

            foreach ($data as $row) {
                $results[$count][] = $row->getName();
                $results[$count][] = $row->getCompleteness();
                $results[$count][] = $renderer->render('Brown298ReportBuilderBundle:Index:BuiltReportList/shared.html.twig', array('report' => $row));
                $results[$count][] = $renderer->render('Brown298ReportBuilderBundle:Index:BuiltReportList/actions.html.twig', array('report' => $row));
                $count += 1;
            }

            return $results;
        };
    }
}
