<?php
namespace Brown298\ReportBuilderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ParameterForm
 * @package Brown298\ReportBuilderBundle\Form
 */
class ParameterForm extends AbstractType
{

    protected $parameters;

    /**
     * @param $parameters
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \Brown298\ReportBuilderBundle\Entity\Filter $parameter */
        foreach ($this->parameters as $parameter) {
            $parameterName = str_ireplace(array(':'), '', $parameter->getValue());
            $comparison    = str_ireplace(array('_'), ' ', $parameter->getType());

            switch ($parameter->getRuntimeFilterType()) {

                case 'Date':
                    $builder->add($parameterName, 'date', array(
                        'label'  => $parameterName . ' ' . $comparison,
                        'widget' => 'single_text',
                        'attr'  => array(
                            'class' => 'value',
                        ),
                        'required'  => false,
                    ));
                    break;
                case 'DateTime':
                    $builder->add($parameterName, 'datetime', array(
                        'label' => $parameterName . ' ' . $comparison,
                        'widget' => 'single_text',
                        'attr' => array(
                            'class' => 'value',
                        ),
                        'required'  => false,
                    ));
                    break;
                case 'Integer':
                    $builder->add($parameterName, 'number', array(
                        'label' => $parameterName . ' ' . $comparison,
                        'attr' => array(
                            'class' => 'value',
                        ),
                        'precision' => 0,
                        'required'  => false,
                    ));
                    break;
                case 'Decimal':
                    $builder->add($parameterName, 'number', array(
                        'label' => $parameterName . ' ' . $comparison,
                        'attr' => array(
                            'class' => 'value',
                        ),
                        'precision' => 2,
                        'required'  => false,
                    ));
                    break;
                case 'Choice':
                    $choices      = array();
                    $choiceString = $parameter->getRuntimeFilterChoices();
                    $rawChoices   = explode(',', $choiceString);
                    $choices[''] = 'Choose an Option';
                    foreach ($rawChoices as $choice) {
                        $choices[trim($choice)] = trim($choice);
                    }

                    $builder
                        ->add($parameterName, 'choice', array(
                            'label'       => $parameterName . ' ' . $comparison,
                            'choices'     => $choices,
                            'attr' => array(
                                'class' => 'value',
                            ),
                            'required' => false,
                        ))
                    ;

                    break;
                default:
                    $builder
                        ->add($parameterName, 'text', array(
                            'label' => $parameterName . ' ' . $comparison,
                            'attr' => array(
                                'class' => 'value',
                            ),
                            'required' => false,
                        ))
                    ;
                    break;
            }
        }

    }
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'report_parameters';
    }
}