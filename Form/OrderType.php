<?php
namespace Brown298\ReportBuilderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class OrderType
 * @package Brown298\ReportBuilderBundle\Form
 */
class OrderType extends AbstractType
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('path', null, array(
                'attr'     => array('class' => 'path'),
                'required' => false,
            ))
            ->add('function', null, array(
                'attr'     => array('class' => 'function'),
                'required' => false,
            ))
            ->add('ascending', 'choice', array(
                'attr'     => array('class' => 'ascending'),
                'required' => false,
                'choices'  => array(1 => 'Ascending', 0 => 'Descending'),
                'label'    => 'Direction',
            ))
        ;

    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                                    'data_class' => 'Brown298\ReportBuilderBundle\Entity\Order',
                               ));
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return 'order';
    }
}