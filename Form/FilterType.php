<?php
namespace Brown298\ReportBuilderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class FilterType
 * @package Brown298\ReportBuilderBundle\Form
 */
class FilterType extends AbstractType
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('path', null, array(
                'attr' => array(
                    'class' => 'path',
                ),
                'required' => false,
            ))
            ->add('function', null, array(
                'attr' => array(
                    'class' => 'function',
                ),
                'required' => false,
            ))
            ->add('type', null, array(
                'attr' => array(
                    'class' => 'type',
                ),
                'required' => false,
            ))
            ->add('value', null, array(
                'attr' => array(
                    'class' => 'value',
                ),
                'required' => false,
            ))
            ->add('second_value', null, array(
                'attr' => array(
                    'class' => 'second_value',
                ),
                'required' => false,
            ))
            ->add('runtimeFilterType', null, array(
                'attr' => array(
                    'class' => 'runtimeFilterType'
                ),
                'required' => false,
            ))
            ->add('runtimeFilterChoices', 'text', array(
                'attr' => array(
                    'class' => 'runtimeFilterChoices',
                ),
                'required' => false,
            ))
        ;
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                                    'data_class' => 'Brown298\ReportBuilderBundle\Entity\Filter',
                               ));
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return 'filter';
    }
}