<?php
namespace Brown298\ReportBuilderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class FieldType
 * @package Brown298\ReportBuilderBundle\Form
 */
class FieldType extends AbstractType
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('path', null, array(
                'attr'     => array('class' => 'path'),
                'required' => false,
            ))
            ->add('function', null, array(
                 'attr'     => array('class' => 'function'),
                 'required' => false,
            ))
            ->add('alias', 'text', array(
                'label'    => 'Column Title',
                'attr'     => array('class' => 'alias'),
                'required' => false,
            ))
            ->add('sortOrder', null, array(
                'attr'     => array('class' => 'sort_order'),
                'required' => false,
            ))
        ;

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                                    'data_class' => 'Brown298\ReportBuilderBundle\Entity\Field',
                               ));
    }


    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return 'field';
    }
}