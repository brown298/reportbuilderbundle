<?php
namespace Brown298\ReportBuilderBundle\Form\Step;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class EntityStepType
 * @package Brown298\ReportBuilderBundle\Form\Step
 */
class EntityStepType extends AbstractStepType
{
    /**
     * @var array
     */
    private $classes = array();

    /**
     * setClasses
     *
     * @param array $classes
     * @return null
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;
    }

    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('entity', 'choice', array(
            'choices'  => $this->getChoices(),
            'required' => false,
            'label'    => 'Base Element',
            'expanded' => true,
        ));

        parent::buildForm($builder, $options);
    }

    /**
     * creates the list of available choices
     *
     * @return array
     */
    protected function getChoices()
    {
        $security = $this->getSecurity();

        $choices = array();
        foreach ($this->classes as $class) {
            if (!$security->isViewingAllowed($class)) {
                continue;
            }

            $treeMetadata  = $this->getTreeMetadata();
            $classMetadata = $treeMetadata->getClassMetadata($class);

            $class = $classMetadata->getClass();
            $label = $classMetadata->getLabel();

            $choices[$class] = $label;
        }

        return $choices;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('validation_groups' => array('entity_step')));
    }

}