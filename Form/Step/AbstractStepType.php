<?php
namespace Brown298\ReportBuilderBundle\Form\Step;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;
use Doctrine\Common\Util\Inflector as DoctrineInflector;
use Brown298\ReportBuilderBundle\Entity\Report;
use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

/**
 * Class AbstractStepType
 * @package Brown298\ReportBuilderBundle\Form\Step
 */
abstract class AbstractStepType extends AbstractType
{
    /**
     * @var Report
     */
    private $report;

    /**
     * @var TreeMetadata
     */
    private $treeMetadata;

    /**
     * @var SecurityInterface
     */
    private $security;

    /**
     * Constructor
     *
     * @param Report                                                             $report
     * @param TreeMetadata                                                       $treeMetadata
     * @param \Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface $security
     */
    public function __construct( Report $report, TreeMetadata $treeMetadata, SecurityInterface $security )
    {
        $this->report       = $report;
        $this->treeMetadata = $treeMetadata;
        $this->security     = $security;
    }

    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('redirect', 'hidden', array(
            'data'          => 'reports_build_step',
            'mapped'        => false,
            'required'      => false,
            'attr'          => array('class' => 'redirect'),
        ));
    }

    /**
     * getReport
     *
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * getTreeMeta
     *
     * @return TreeMetadata
     */
    public function getTreeMetadata()
    {
        return $this->treeMetadata;
    }

    /**
     * getSecurity
     *
     * @return SecurityService
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * getStepKey
     *
     * @return string
     */
    public function getStepKey()
    {
        $key = get_class($this);
        $key = substr($key, strrpos($key, '\\') + 1);
        $key = preg_replace('/Type$/', '', $key);
        $key = DoctrineInflector::tableize($key);

        return $key;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        $thisClass = get_class($this);
        $thisClass = str_replace('\\', '_', $thisClass);
        $thisClass = strtolower($thisClass);

        return $thisClass;
    }
}