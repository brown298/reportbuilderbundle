var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 * OrderStepInterface
 *
 * @param targetElement
 * @param config
 * @constructor
 */
brown298.OrderStepInterface = function(targetElement, config) {
    var self = this;
    this.config = jQuery.extend({
        'treeFilter': '.tree'
    }, this.config);

    /**
     * getTree
     *
     * @type {*}
     */
    self.getTree = brown298.memoize(function() {
        var el_ = targetElement.find(self.config.treeFilter);
        var tree = new brown298.Tree(el_, self);

        var callback = function(event, field) {
            var list = self.getList();
            var path = field.getPath();
            var func = field.getFunc();

            if (field.getCheckbox().isChecked()) {
                list.addItem(path, func);
            } else {
                list.removeItemByPath(path, func);
            }
        };

        tree.addCheckboxes(callback);

        return tree;
    });

    self.getList = brown298.memoize(function() {
        var el_ = targetElement.find('ul.items');
        var list = new brown298.OrderStepList(el_, self);
        return list;
    });

    self.updateCheckboxes = function() {
        var tree = self.getTree();
        tree.uncheckFields();

        var list = self.getList();
        var items = list.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            var path = items[i].getPath();
            var func = items[i].getFunc();
            var field = tree.findField(path, func);

            if (field) {
                field.getCheckbox().setChecked(true);
            }
        }
    };

    var initialize = function() {
        self.getTree();
        self.getList();

        self.updateCheckboxes();

        targetElement.show();
    };

    initialize();
};