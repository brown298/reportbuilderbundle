var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 * TreeGroup
 *
 * @param targetElement
 * @param config
 * @constructor
 */
brown298.TreeGroup = function(targetElement, config) {
    var self = this;
    this.config = jQuery.extend({
    }, this.config);

    this.parentField = null;
    this.tree = null;

    this.setParentField = function(parentField) {
        self.parentField = parentField;
    };

    this.setTree = function(tree) {
        self.tree = tree;
    };

    this.getParentField = function() {
        return self.parentField;
    };

    this.getTree = function() {
        return self.tree;
    };

    this.getFieldsFlat = brown298.memoize(function() {
        var fields = self.getFields();
        var fieldsFlat = [];

        for (var i = 0, length = fields.length; i < length; i++) {

            fieldsFlat.push(fields[i]);

            var group = fields[i].getGroup();
            if (group) {
                var fieldsFlat_ = group.getFieldsFlat();
                fieldsFlat = fieldsFlat.concat(fieldsFlat_);
            }
        }

        return fieldsFlat;
    });

    this.addCheckboxes = function(callback) {
        var fields = self.getFieldsFlat();
        for (var i = 0, length = fields.length; i < length; i++) {
            if (!fields[i].isParent()) {
                fields[i].addCheckbox(callback);
            }
        }
    };

    this.addLinks = function(callback) {
        var fields = self.getFieldsFlat();
        for (var i = 0, length = fields.length; i < length; i++) {
            if (!fields[i].isParent()) {
                fields[i].addLink(callback);
            }
        }
    };

    this.uncheckFields = function() {
        var fields = self.getFieldsFlat();
        for (var i = 0, length = fields.length; i < length; i++) {
            if (!fields[i].isParent()) {
                fields[i].getCheckbox().setChecked(false);
            }
        }
    };

    this.collapseFields = function(animate) {
        var fields = self.getFieldsFlat();
        for (var i = 0, length = fields.length; i < length; i++) {
            if (fields[i].isParent()) {
                fields[i].collapse(animate);
            }
        }
    };

    this.findField = brown298.memoize(function(path, func) {
        var fields = self.getFieldsFlat();

        for (var i = 0, length = fields.length; i < length; i++) {

            var path_ = fields[i].getPath();
            var func_ = fields[i].getFunc();

            if (path_ == path && func_ == func) {
                return fields[i];
            }
        }

        return null;
    });

    this.getRootTree = brown298.memoize(function() {
        var tree = self.getTree();

        if (tree !== null) {
            return tree;
        }

        return self.getParentField().getParentGroup().getRootTree();
    });

    this.show = function(animate, callback) {
        if (animate) {
            if (callback === undefined) {
                var callback = function() {};
            }

            targetElement.slideDown('fast', callback);
        } else {
            targetElement.show();
        }
    };

    this.hide = function(animate, callback) {
        if (animate) {
            if (callback === undefined) {
                var callback = function() {};
            }

            targetElement.slideUp('fast', callback);
        } else {
            targetElement.hide();
        }
    };

    this.getFields = brown298.memoize(function() {
        var fields = [];

        targetElement.children('ul').children('li').each(function() {
            var el_ = jQuery(this);

            var field = new brown298.TreeField(el_, self);

            fields.push(field);
        });

        return fields;
    });

    /**
     * initialize the class
     */
    this.initialize = function() {
        self.getFields();
    };

    this.initialize();
};