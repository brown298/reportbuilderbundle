var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 *
 * @param targetElement
 * @param interf
 * @constructor
 */
brown298.OrderStepList = function(targetElement, interf, config) {
    var self      = this
    this.config = jQuery.extend({
    }, this.config);
    this.itemIdx   = null;
    this.controls = null;

    /**
     * getItems
     * @returns {Array}
     */
    this.getItems = function() {
        var items = [];

        targetElement.children('li').each(function() {

            var el_ = jQuery(this);
            var item = el_.data('orderStepItem');

            if (!item) {
                var idx = self.createItemIdx();

                try {
                    item = new brown298.OrderStepItem(el_, self, idx);
                } catch (e) {
                    el_.remove();
                    return true;
                }

                el_.data('orderStepItem', item);
            }

            items.push(item);
        });

        return items;
    };

    /**
     * getItem
     * @param path
     * @param func
     * @returns {*}
     */
    this.getItem = function(path, func) {
        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            if (path == items[i].getPath() && func == items[i].getFunc()) {
                return items[i];
            }
        }

        return null;
    };

    this.hasItem = function(path, func) {
        return self.getItem(path, func) !== null;
    };

    this.getInterface = function() {
        return interf;
    };

    this.addItem = function(path, func) {
        if (self.hasItem(path, func)) {
            return;
        }

        var html = targetElement.attr('data-prototype');
        var idx = self.createItemIdx();
        html = html.replace(/__name__/g, idx);

        var li = jQuery('<li></li>');
        li.append(html);

        var item = new brown298.OrderStepItem(li, self, idx, path, func);
        li.data('orderStepItem', item);

        targetElement.append(li);

        self.resetSortOrder();
        self.resetControls();
        self.resetEmptyMessage();
        self.getInterface().updateCheckboxes();
    };

    this.removeItem = function(item) {
        item.remove();

        self.resetSortOrder();
        self.resetControls();
        self.resetEmptyMessage();
        self.getInterface().updateCheckboxes();
    };

    this.removeAllItems = function() {
        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            items[i].remove();
        }

        self.resetControls();
        self.resetEmptyMessage();
        self.getInterface().updateCheckboxes();
    };

    this.removeItemByPath = function(path, func) {
        var item = self.getItem(path, func);

        if (item) {
            self.removeItem(item);
        }
    };

    this.resetSortOrder = function() {
        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            items[i].setSortOrder(i + 1);
        }
    };

    this.resetEmptyMessage = function() {
        var items = self.getItems();
        var emptyMessage = getEmptyMessage();

        if (items.length) {
            emptyMessage.hide();
        } else {
            emptyMessage.show();
        }
    };

    this.resetControls = function() {
        var items = self.getItems();

        if (items.length) {
            self.controls.show();
        } else {
            self.controls.hide();
        }
    };

    this.createItemIdx = function() {
        if (self.itemIdx === null) {
            if (targetElement.children('li').length > 1) {
                self.itemIdx = targetElement.children('li').length - 1;
            } else {
                self.itemIdx = 0;
            }
        } else {
            self.itemIdx++;
        }

        return self.itemIdx;
    };

    var getEmptyMessage = brown298.memoize(function() {
        var emptyMessage = jQuery('<p></p>');
        emptyMessage.addClass('empty');
        emptyMessage.hide();

        var text = 'No fields are chosen';
        emptyMessage.html(text);

        targetElement.parent().before(emptyMessage);

        return emptyMessage;
    });

    var getControls = brown298.memoize(function() {
        var controls = jQuery('<p></p>');
        controls.addClass('list-controls');
        controls.hide();

        targetElement.parent().prev().before(controls);

        self.controls = controls;
        return self.controls;
    });

    var addControl = function(text, callback, title) {
        var link = brown298.getLink(text, callback);
        link.addClass('control');
        if (title) {
            link.attr('title', title);
        }

        var controls = getControls();
        controls.append(' ').append(link);

        return link;
    };

    this.initializeSortable = function() {
        targetElement.sortable({
            axis: 'y',
            handle: '.move',
            update: function(event, ui) {
                self.resetSortOrder();
            }
        });
    };

    this.initializeReset = function() {
        var control = addControl('Reset', self.removeAllItems, 'Delete all');

        control.hover(function() {
            targetElement.addClass('reset-hovering');
        }, function() {
            targetElement.removeClass('reset-hovering');
        });
    };

    /**
     * initialize
     */
    this.initialize = function() {
        self.initializeSortable();
        self.initializeReset();
        self.resetSortOrder();
        self.resetControls();
        self.resetEmptyMessage();
    };

    this.initialize();
};