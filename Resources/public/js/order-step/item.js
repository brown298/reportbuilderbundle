var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 *
 * @param targetElement
 * @param list
 * @param idx
 * @param initialPath
 * @param initialFunc
 * @constructor
 */
brown298.OrderStepItem = function(targetElement, list, idx, initialPath, initialFunc) {
    var self = this;

    this.getIdx = function() {
        return idx;
    };

    this.getPath = brown298.memoize(function() {
        return self.getInputValue('path');
    });

    this.getFunc = brown298.memoize(function() {
        return self.getInputValue('function');
    });

    this.getField = brown298.memoize(function() {
        var path = self.getPath();
        var func = self.getFunc();
        var field = list.getInterface().getTree().findField(path, func);

        if (field === null) {
            throw new Error('Invalid field');
        }

        return field;
    });

    this.getInputValue = brown298.memoize(function(key) {
        var val = self.getInput(key).val();

        if (val === undefined || !val) {
            val = null;
        }

        return val;
    });

    this.setSortOrder = function(sortOrder) {
        self.getInput('sort_order').val(sortOrder);
    };

    this.getInput = brown298.memoize(function(name) {
        var el_ = targetElement.find('input.'+name);
        return el_;
    });

    this.getSelect = brown298.memoize(function(name) {
        var el_ = targetElement.find('select.'+name);
        return el_;
    });

    this.remove = function() {
        targetElement.remove();
    };

    this.addLabel = function() {

        // Get text
        var field = self.getField();
        var text = field.getPropLabel().text();
        text = jQuery.trim(text);

        // Get prefix
        var prefixText = field.getLabelPrefix();
        if (prefixText) {
            var labelPrefix = jQuery('<span></span>');
            labelPrefix.html(prefixText);
            labelPrefix.addClass('prefix');
        }

        // Element
        var label = jQuery('<span></span>');
        label.html(text);
        if (labelPrefix) {
            label.append(' ').append(labelPrefix);
        }
        label.addClass('item-label');
        targetElement.append(label);
        targetElement.find('> div').hide();
    };

    this.addMoveHandle = function() {
        var move = jQuery('<span></span>');
        move.addClass('move');
        targetElement.append(move);
    };

    this.addAscendingField = function() {
        var ascending = self.getSelect('ascending');
        targetElement.append(ascending);
    };

    this.addRemoveButton = function() {
        var remove = jQuery('<span></span>');
        remove.addClass('remove');
        remove.attr('title', 'Remove from report');
        remove.click(function() {
            list.removeItem(self);
        });
        targetElement.append(remove);
    };

    /**
     * initialize
     */
    this.initialize = function() {
        if (initialPath !== undefined) {
            self.getInput('path').val(initialPath);
        }
        if (initialFunc !== undefined) {
            self.getInput('function').val(initialFunc);
        }

        this.addMoveHandle();
        this.addLabel();
        this.addRemoveButton();
        this.addAscendingField();
    };

    this.initialize();
};