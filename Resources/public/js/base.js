var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 * remember a function by caching its output
 * @param func
 * @returns {Function}
 */
brown298.memoize = function(func) {
    return function() {

        // Build hash from arguments
        var args = Array.prototype.slice.call(arguments);
        var hashArgs = [];
        for (var i = 0; i < args.length; i++) {
            var arg = args[i];

            if (typeof(arg) === 'object') {
                arg = JSON.stringify(arg);
            }

            hashArgs.push(arg);
        }
        var hash = hashArgs.join('|');

        // Save function output to cache
        if (func.memo === undefined) {
            func.memo = {};
        }
        if (func.memo[hash] === undefined) {
            func.memo[hash] = func.apply(this, args);
        }

        // Return cache
        return func.memo[hash];
    };
}

/**
 * getLink
 * s
 * @param text
 * @param callback
 * @returns {*}
 */
brown298.getLink = function(text, callback) {

    var link = jQuery('<a></a>');
    link.attr('href', 'javascript:void(0)');
    link.html(text);
    link.click(callback);

    return link;
};

/**
 * stepControl
 */
brown298.stepControl = function() {
    // If the continue button is pressed, continue
    var form = jQuery('form.report-build');
    if (form) {
        var viewButton = form.find('.view');
        var continueButton = form.find('.continue');
        var redirect = form.find('.redirect');

        viewButton.click(function() {
            redirect.val('reports_view_index');
        });

        continueButton.click(function() {
            redirect.val('reports_build_next');
        });
    }
}

jQuery(document).ready(brown298.stepControl);