var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 *
 * @param targetElement
 * @param config
 * @constructor
 */
brown298.Result = function(targetElement, config) {
    var self = this;
    this.config = jQuery.extend({
    }, this.config);

    /**
     * gets the target element
     * @returns {*}
     */
    this.getEl = function() {
        return targetElement;
    };

    /**
     * initialize
     */
    this.initialize = function() {
    };

    this.initialize();
};