var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 *
 * @param targetElement
 * @param config
 */
brown298.Tree = function(targetElement, config) {
    var self = this;
    this.config = jQuery.extend({
        'countFieldFilter': '.count',
        'groupFieldFilter': '.group',
        'listControlClass': 'list-controls',
        'controlClass':     'control'
    }, this.config);

    /**
     * handles collapsing an element in a tree
     * @param animate
     */
    this.collapseFields = function(animate) {
        self.getGroup().collapseFields(animate);
    };

    /**
     * searches for a field in the tree
     * @param path
     * @param func
     * @returns {*}
     */
    this.findField = function(path, func) {
        var field = self.getGroup().findField(path, func);
        return field;
    };

    /**
     * finds the count field item in the tree
     * @type {*}
     */
    this.getCountField = brown298.memoize(function() {
        var element = targetElement.find(self.config.countFieldFilter);
        return element;
    });

    /**
     * gets a group in the tree
     * @type {*}
     */
    this.getGroup = brown298.memoize(function() {
        var element = targetElement.children(self.config.groupFieldFilter);

        var group = new brown298.TreeGroup(element);
        group.setTree(self);

        return group;
    });

    this.addCheckboxes = function(callback) {
        self.getGroup().addCheckboxes(callback);
    };

    this.addLinks = function(callback) {
        self.getGroup().addLinks(callback);
    };

    this.getElement = function() {
        return targetElement;
    };

    this.uncheckFields = function() {
        self.getGroup().uncheckFields();
    };

    this.addControl = function(text, callback, title) {

        var link = brown298.getLink(text, callback);
        link.addClass(self.config.controlClass);

        if (title) {
            link.attr('title', title);
        }

        var controls = getControls();
        controls.append(' ').append(link);

        return link;
    };

    var getControls = brown298.memoize(function() {
        var controls = jQuery('<p></p>');
        controls.addClass(self.config.listControlClass);

        targetElement.parent().prev().before(controls);

        self.controls = controls;
        return self.controls;
    });

    this.initialize = function() {
        self.getGroup();
        self.collapseFields(false);
    };

    this.initialize();
};