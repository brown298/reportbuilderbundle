var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 *
 * @param el
 * @param filter
 * @constructor
 */
brown298.FilterStepType = function(el, filter) {
    var self = this;

    this.getEl = function() {
        return el;
    };

    this.getFilter = function() {
        return filter;
    };

    this.getKey = brown298.memoize(function() {
        return el.attr('data-key');
    });

    this.hide = function() {
        self.getEl().hide();
    };

    this.show = function() {
        self.getEl().show();
    };

    var getInterface = brown298.memoize(function() {
        return self.getFilter().getItem().getList().getInterface();
    });

    var getCallback = function(action) {
        var key = self.getKey();
        var callback = getInterface().getCallbackRegistry().get(key, action);
        return callback;
    };

    /**
     * runCallback
     *
     * @param action
     * @returns {null}
     */
    var runCallback = function(action) {
        var callback = getCallback(action);

        if (!callback) {
            return null;
        }

        return callback(self);
    };

    /**
     * init
     */
    this.init = function() {
        return runCallback('init');
    };

    this.read = function() {
        return runCallback('read');
    };

    this.write = function() {
        return runCallback('write');
    };

    this.empty = function() {
        return runCallback('empty');
    };

    this.focus = function() {
        return runCallback('focus');
    };

    /**
     * applyEvents
     * @returns {null}
     */
    var applyEvents = function() {
        var applyEvents = getCallback('applyEvents');

        if (!applyEvents) {
            return null;
        }

        var callback = function() {
            self.write();
        };

        return applyEvents(self, callback);
    };

    /**
     * initialize
     */
    var initialize = function() {
        self.empty();
        self.init();

        applyEvents();
    };

    initialize();
};