var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 *
 * @param targetElement
 * @param item
 * @constructor
 */
brown298.FilterStepFilter = function(targetElement, item) {
    var self = this;
    this.config = jQuery.extend({
    }, this.config);
    this.getItem = function() {
        return item;
    };

    /**
     * getSelect
     *
     * @type {Function}
     */
    this.getSelect = brown298.memoize(function() {
        var select = targetElement.find('select.filter-type');
        return select;
    });

    /**
     * getTypes
     * @type {Function}
     */
    this.getTypes = brown298.memoize(function() {
        var types = [];

        targetElement.find('.filter-type-form').each(function() {
            var el_ = jQuery(this);
            var type = new brown298.FilterStepType(el_, self);
            types.push(type);
        });

        return types;
    });

    /**
     * getType
     *
     * @param key
     * @returns {null}
     */
    this.getType = function(key) {
        var types = self.getTypes();

        for (var i = 0, length = types.length; i < length; i++) {
            var thisKey = types[i].getKey();

            if (thisKey === key) {
                return types[i];
            }
        }

        return null;
    };

    /**
     * runtimeFilterConfigure
     *
     */
    this.runtimeFilterConfigure = function() {
        var value = jQuery(this).val();

        if (value == '') {
            return;
        }

        if (value.match(/^:.*/)) {
            targetElement.find('.runtime-filter-config').show();
        } else {
            targetElement.find('.runtime-filter-config').hide();
        }
    }

    /**
     * emptyTypes
     */
    var emptyTypes = function() {
        var types = self.getTypes();
        for (var i = 0, length = types.length; i < length; i++) {
            types[i].empty();
        }
    };

    /**
     * switchToType
     * @param key
     * @returns {null}
     */
    var switchToType = function(key) {
        var types = self.getTypes();
        for (var i = 0, length = types.length; i < length; i++) {
            types[i].hide();
        }

        var type = self.getType(key);
        type.show();

        return type;
    };

    /**
     * initialize
     */
    var initialize = function() {

        emptyTypes();

        // Set initial select value
        var item        = self.getItem();
        var typeInput   = item.getInput('type');
        var current     = typeInput.val();
        var select      = self.getSelect();
        var value       = item.getInput('value');
        var secondValue = item.getInput('second_value');
        select.val(current);

        var type = self.getType(current);
        if (type) {
            type.read();
            switchToType(current);
        }

        select.change(function() {

            typeInput.val(select.val());

            emptyTypes();
            item.emptyValueInputs();

            var current = select.val();
            if (current) {
                var type = switchToType(current);
                type.focus();
            }
        });

        value.change(self.runtimeFilterConfigure);
        secondValue.change(self.runtimeFilterConfigure);
        value.focus(self.runtimeFilterConfigure);
        secondValue.focus(self.runtimeFilterConfigure);

        value.focus();

        item.endLoading();

        targetElement.show();
    };

    initialize();
};