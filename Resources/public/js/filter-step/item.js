var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 *
 * @param targetElement
 * @param list
 * @param idx
 * @param initialPath
 * @param initialFunc
 * @constructor
 */
brown298.FilterStepItem = function(targetElement, list, idx, initialPath, initialFunc, config) {
    var self = this;
    this.config = jQuery.extend({
    }, this.config);
    this.el = targetElement;
    this.filterTypeSelect = null;

    /**
     * getIdx
     *
     * @returns {*}
     */
    this.getIdx = function() {
        return idx;
    };

    /**
     * getList
     *
     * @returns {*}
     */
    this.getList = function() {
        return list;
    };

    /**
     * getPath
     *
     * @type {Function}
     */
    this.getPath = brown298.memoize(function() {
        return self.getInputValue('path');
    });

    /**
     * getFunc
     *
     * @type {Function}
     */
    this.getFunc = brown298.memoize(function() {
        return self.getInputValue('function');
    });

    /**
     * getField
     *
     * @type {Function}
     */
    this.getField = brown298.memoize(function() {
        var path = self.getPath();
        var func = self.getFunc();
        var field = list.getInterface().getTree().findField(path, func);

        if (field === null) {
            throw new Error('Invalid field');
        }

        return field;
    });

    /**
     * getInputValue
     *
     * @type {Function}
     */
    this.getInputValue = brown298.memoize(function(key) {
        var val = self.getInput(key).val();

        if (val === undefined || !val) {
            val = null;
        }

        return val;
    });

    /**
     * setSortOrder
     * @param sortOrder
     */
    this.setSortOrder = function(sortOrder) {
        self.getInput('sort_order').val(sortOrder);
    };

    /**
     * getLoading
     * @type {Function}
     */
    var getLoading = brown298.memoize(function() {
        var loading = jQuery('<span></span>');
        loading.addClass('loading');
        targetElement.append(loading);

        return loading;
    });

    /**
     * startLoading
     */
    this.startLoading = function() {
        var loading = getLoading();
        loading.show();
    };

    /**
     * endLoading
     */
    this.endLoading = function() {
        var loading = getLoading();
        loading.hide();
    };

    /**
     * remove
     */
    this.remove = function() {
        targetElement.remove();
    };

    /**
     * getInput
     *
     * @param key
     * @returns {*}
     */
    this.getInput = function(key) {
        var input = targetElement.find('input.' + key);
        return input;
    };

    /**
     * emptyValueInputs
     */
    this.emptyValueInputs = function() {
        self.getInput('value').val('');
        self.getInput('second_value').val('');
    };

    /**
     *
     * @type {Function}
     */
    this.getFilterEl = brown298.memoize(function() {
        var filterEl = jQuery('<div></div>');
        filterEl.addClass('form');
        targetElement.append(filterEl);
        return filterEl;
    });

    /**
     *
     * @param html
     */
    this.createFilter = function(html) {
        var filterEl = self.getFilterEl();
        filterEl.append(html);

        self.filter = new brown298.FilterStepFilter(filterEl, self);
    };

    /**
     * adds a move handle to the element
     */
    this.addMoveHandle = function() {
        var move = jQuery('<span></span>');
        move.addClass('move');
        targetElement.append(move);
    };

    /**
     * adds a label to the element
     */
    this.addLabel = function() {
        // Get text
        var field = self.getField();
        var text = field.getPropLabel().text();
        text = jQuery.trim(text);

        // Get prefix
        var prefixText = field.getLabelPrefix();
        if (prefixText) {
            var labelPrefix = jQuery('<span></span>');
            labelPrefix.html(prefixText);
            labelPrefix.addClass('prefix');
        }

        // Element
        var label = jQuery('<span></span>');
        label.html(text);
        if (labelPrefix) {
            label.append(' ').append(labelPrefix);
        }
        label.addClass('item-label');
        targetElement.append(label)
        targetElement.find('div').hide();
    };

    /**
     * addErrorMessages
     * @returns {boolean}
     */
    this.addErrorMessages = function() {

        var sources = targetElement.find('.help-inline');
        if (!sources.length) {
            return false;
        }

        var errors = jQuery('<ul></ul>');
        errors.addClass('errors');

        sources.each(function() {
            var message = jQuery(this).html();

            var error = jQuery('<li></li>');
            error.addClass('error');
            error.html(message);
            errors.append(error);
        });

        var filterEl = self.getFilterEl();
        filterEl.after(errors);
    };

    /**
     * addRemoveButton
     */
    this.addRemoveButton = function() {
        var remove = jQuery('<span></span>');
        remove.addClass('remove');
        remove.attr('title', 'Remove from report');
        remove.click(function() {
            list.removeItem(self);
        });
        targetElement.append(remove);
    };

    /**
     * initialize
     */
    this.initialize = function() {
        if (initialPath !== undefined) {
            self.getInput('path').val(initialPath);
        }
        if (initialFunc !== undefined) {
            self.getInput('function').val(initialFunc);
        }

        self.addMoveHandle();
        self.addLabel();
        self.addRemoveButton();
        self.addErrorMessages();
    };

    this.initialize();
};