var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 *
 * @param targetElement
 * @param interf
 * @param config
 * @constructor
 */
brown298.FilterStepList = function(targetElement, interf, config) {
    var self = this;
    this.config = jQuery.extend({
    }, this.config);

    this.itemIdx = null;
    this.controls = null;

    /**
     * gets an array of the items
     * @returns {Array}
     */
    this.getItems = function() {
        var items = [];
        var newItems = [];

        targetElement.children('li').each(function() {

            var element = jQuery(this);
            var item = element.data('filterStepItem');

            if (!item) {
                var idx = self.createItemIdx();

                try {
                    item = new brown298.FilterStepItem(element, self, idx);
                } catch (e) {
                    element.remove();
                    return true;
                }

                element.data('filterStepItem', item);

                newItems.push(item);
            }

            items.push(item);
        });

        if (newItems.length) {
            initializeItemFilters(newItems);
        }

        return items;
    };

    var initializeItemFilters = function(items) {

        if (!jQuery.isArray(items)) {
            items = [ items ];
        }

        var params = self.getInterface().getParams();

        var data = { class: params.class };
        data.items = [];

        for (var i = 0, length = items.length; i < length; i++) {
            var item = items[i];

            var itemData = {
                idx: item.getIdx(),
                path: item.getPath() || '',
                func: item.getFunc() || ''
            };

            data.items.push(itemData);
        }

        var complete = function(data) {
            var output = data.responseText;
            var loggedOut = (output.match(/action="\/login_check"/g));

            if (loggedOut) {
                window.location.href = window.location.href;
            }
        }

        var success = function(data) {

            // Every succeeful response needs to have data.success set to true
            if (!data.success) {

                for (var i = 0, length = items.length; i < length; i++) {
                    var item = items[i];

                    var message = 'An error occurred gathering the filter';
                    message = '<span class="error">' + message + '</span>';

                    item.endLoading();
                    item.createFilter(message);
                }

                return;
            }

            var returnedItems = data.items;

            for (var i = 0, length = returnedItems.length; i < length; i++) {

                var html = returnedItems[i].html;
                var idx  = returnedItems[i].idx;

                var item = self.getItemByIdx(idx);

                item.createFilter(html);
            }
        };

        for (var i = 0, length = items.length; i < length; i++) {
            var item = items[i];
            item.startLoading();
        }

        jQuery.ajax(params.filterFormUrl, {
            type: 'POST',
            dataType: 'json',
            data: data,
            success: success,
            complete: complete
        });
    };

    this.getItemByIdx = function(idx) {
        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            if (idx == items[i].getIdx()) {
                return items[i];
            }
        }

        return null;
    };

    this.getInterface = function() {
        return interf;
    };

    this.addItem = function(path, func) {
        var html = targetElement.attr('data-prototype');
        var idx  = self.createItemIdx();
        html = html.replace(/__name__/g, idx);

        var li = jQuery('<li></li>');
        li.append(html);

        var item = new brown298.FilterStepItem(li, self, idx, path, func);
        initializeItemFilters(item);
        li.data('filterStepItem', item);

        targetElement.append(li);

        self.resetSortOrder();
        self.resetControls();
        self.resetEmptyMessage();
    };

    this.removeItem = function(item) {
        item.remove();

        self.resetSortOrder();
        self.resetControls();
        self.resetEmptyMessage();
    };

    var removeAllItems = function() {
        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            items[i].remove();
        }

        self.resetControls();
        self.resetEmptyMessage();
    };

    this.resetSortOrder = function() {
        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            items[i].setSortOrder(i + 1);
        }
    };

    var getEmptyMessage = brown298.memoize(function() {
        var emptyMessage = jQuery('<p></p>');
        emptyMessage.addClass('empty');
        emptyMessage.hide();

        var text = 'No fields are chosen';
        emptyMessage.html(text);

        targetElement.parent().before(emptyMessage);

        return emptyMessage;
    });

    this.resetEmptyMessage = function() {
        var items = self.getItems();
        var emptyMessage = getEmptyMessage();

        if (items.length) {
            emptyMessage.hide();
        } else {
            emptyMessage.show();
        }
    };

    this.resetControls = function() {
        var items = self.getItems();
        if (items.length) {
            self.controls.show();
        } else {
            self.controls.hide();
        }
    };

    this.createItemIdx = function() {
        if (self.itemIdx === null) {
            self.itemIdx = 1;
        } else {
            self.itemIdx++;
        }

        return self.itemIdx;
    };

    var addButton = function(text, callback) {
        var container = targetElement.parent();
        var buttons = container.next('.buttons');

        if (!buttons.length) {
            buttons = jQuery('<p></p>');
            buttons.addClass('buttons');
            container.after(buttons);
        }

        var link = brown298.getLink(text, callback);
        link.addClass('btn');
        buttons.append(link);
    };

    var getControls = brown298.memoize(function() {
        var controls = jQuery('<p></p>');
        controls.addClass('list-controls');
        controls.hide();

        targetElement.parent().prev().before(controls);

        self.controls = controls;
        return self.controls;
    });

    var showTreePane = function() {
        self.getInterface().showTreePane();
    };

    var addControl = function(text, callback, title) {
        var link = brown298.getLink(text, callback);
        link.addClass('control');
        if (title) {
            link.attr('title', title);
        }

        var controls = getControls();
        controls.append(' ').append(link);

        return link;
    };

    var initialize = function() {

        var initializeSortable = function() {
            targetElement.sortable({
                axis: 'y',
                handle: '.move',
                update: function(event, ui) {
                    self.resetSortOrder();
                }
            });
        };

        var initializeReset = function() {
            var control = addControl('Reset', removeAllItems, 'Delete all');

            control.hover(function() {
                targetElement.addClass('reset-hovering');
            }, function() {
                targetElement.removeClass('reset-hovering');
            });
        };

        var initializeAddLink = function() {
            addButton('Create a filter', showTreePane);
        };

        initializeSortable();
        initializeReset();
        initializeAddLink();

        self.resetSortOrder();
        self.resetControls();
        self.resetEmptyMessage();
    };

    initialize();
};