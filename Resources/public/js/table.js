jQuery(function() {

    jQuery.extend($.fn.dataTableExt.oStdClasses, {
        sWrapper: 'dataTables_wrapper form-inline'
    });

    if ($.fn.DataTable.TableTools) {
        jQuery.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group export-button",
            "collection": {
                "container": "dropdown-menu",
                "background": "",
                "buttons": {
                    "normal": "",
                    "disabled": "disabled"
                }
            },
            "buttons": {
                "normal": "btn dropdown-toggle",
                "disabled": "btn dropdown-toggle disabled"
            }
        });

        // Have the collection use a bootstrap compatible dropdown
        jQuery.extend(true, $.fn.DataTable.TableTools.DEFAULTS.oTags, {
            "collection": {
                "container": "ul",
                "button": "li",
                "liner": "a"
            }
        });
    }
});