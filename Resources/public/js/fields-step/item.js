var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 * FieldStepItem
 *
 * @param targetElement
 * @param list
 * @param idx
 * @param initialPath
 * @param initialFunc
 * @param config
 * @constructor
 */
brown298.FieldsStepItem = function(targetElement, list, idx, initialPath, initialFunc, config) {
    var self = this;
    this.idx = idx;
    this.config = jQuery.extend({
        'treeFilterClass':  '.tree',
        'childFilterClass': 'ul.items'
    }, config);

    /**
     * getIdx
     * @returns {*}
     */
    this.getIdx = function() {
        return self.idx;
    };

    /**
     * getPath
     * @type {*}
     */
    this.getPath = brown298.memoize(function() {
        return self.getInputValue('path');
    });

    /**
     * getFunc
     * @type {*}
     */
    this.getFunc = brown298.memoize(function() {
        return self.getInputValue('function');
    });

    /**
     * getAlias
     * @type {Function}
     */
    this.getAlias = brown298.memoize(function() {
        return self.getInputValue('alias');
    });

    /**
     * getField
     * @type {*}
     */
    this.getField = brown298.memoize(function() {
        var path = self.getPath();
        var func = self.getFunc();
        var field = list.getInterface().getTree().findField(path, func);
        if (field === null) {
            field = list.getInterface().getTree().findField(path, null);
            if (field === null) {
                throw new Error('Invalid field');
            }
        }

        return field;
    });

    /**
     * getInputValue
     * @type {*}
     */
    this.getInputValue = brown298.memoize(function(key) {
        var val = self.getInput(key).val();

        if (val === undefined || !val) {
            val = null;
        }

        return val;
    });

    /**
     * isCountField
     * @type {*}
     */
    this.isCountField = brown298.memoize(function() {
        return (self.getInputValue('function') === 'count');
    });

    /**
     * setSortOrder
     * @param sortOrder
     */
    this.setSortOrder = function(sortOrder) {
        self.getInput('sort_order').val(sortOrder);
    };

    /**
     * getInput
     * @type {*}
     */
    this.getInput = brown298.memoize(function(name) {
        var element = targetElement.find('input.'+name);
        return element;
    });

    /**
     * remove
     */
    this.remove = function() {
        targetElement.remove();
    };

    /**
     * addMoveHandle
     */
    this.addMoveHandle = function() {
        var move = jQuery('<span></span>');
        move.addClass('move');
        targetElement.append(move);
    };

    /**
     * addLabel
     */
    this.addLabel = function() {

       /* get text */
        var field = self.getField();
        var text = field.getPropLabel().text();
        text = jQuery.trim(text);

        /* Get prefix */
        var prefixText = field.getLabelPrefix();
        if (prefixText) {
            var labelPrefix = jQuery('<span></span>');
            labelPrefix.html(prefixText);
            labelPrefix.addClass('prefix');
        }

        /* Element */
        var label = jQuery('<span></span>');
        label.html(text);
        if (labelPrefix) {
            label.append(' ').append(labelPrefix);
        }
        label.addClass('item-label');
        targetElement.append(label);
        targetElement.find('> div').hide();

        /* show the alias form */
        targetElement.find('> div.control-group-block-alias').show();
        targetElement.find('div.control-group-block-alias div.control-group-alias').show();

        self.createFunctionEdit(targetElement);
    };

    /**
     * createFunctionEdit
     */
    this.createFunctionEdit = function(targetElement) {
        var self               = this;
        var idx                = self.getIdx();
        var func               = self.getFunc();
        var functionEditButton = jQuery('<span class="glyphicon glyphicon-cog"></span>');
        var modalElement       = jQuery('<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="functionEditModal_'
            + idx + '"><div class="modal-dialog modal-sm><div class="modal-content"><label for="functionEditFunction_' + idx
            + '"  >Function</label><input id="functionEditInput_' + idx + '" class="functionEdit" /></div><div class="modal-footer">'
            + '<button id="functionEditSave_' + idx + '" type="button" class="btn btn-primary">Save changes</button></div></div></div>');

        jQuery('#functionEditModal_' + idx).modal({show: false});
        functionEditButton.on('click', function() {
            jQuery('#functionEditModal_' + idx).modal('show');
            jQuery('#functionEditInput_' + idx).val(func);

            jQuery('.modal-backdrop').on('click', function() {
                jQuery('#functionEditModal_' + idx).modal('hide');
            });
        })

        targetElement.append(modalElement);
        targetElement.append(functionEditButton);

        jQuery('#functionEditSave_' + idx).on('click', function() {
            var currentValue = jQuery('#functionEditInput_' + idx).val();
            self.getInput('function').val(currentValue);
            jQuery('#functionEditModal_' + idx).modal('hide');
        });

    },

    /**
     * addRemoveButton
     *
     * adds a button to remove the current selected item
     */
    this.addRemoveButton = function() {
        var remove = jQuery('<span></span>');
        remove.addClass('remove');
        remove.attr('title', 'Remove from report');
        remove.click(function() {
            list.removeItem(self);
        });
        targetElement.append(remove);
    };

    /**
     * initialize
     */
    this.initialize = function() {
        if (initialPath !== undefined) {
            self.getInput('path').val(initialPath);
        }

        if (initialFunc !== undefined) {
            self.getInput('function').val(initialFunc);
        }

        self.addMoveHandle();
        self.addLabel();
        self.addRemoveButton();
    };

    this.initialize();
};