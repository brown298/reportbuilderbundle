# Symfony2/Doctrine2 Adhoc Report Builder Bundle

[![Build Status](http://rbsolutions.dyndns.org:8080/buildStatus/icon?job=ReportBuilderBundle)](http://rbsolutions.dyndns.org:8080/job/ReportBuilderBundle/)

This bundle adds a dynamic reporting engine to your Symfony2/Doctrine project

## Install
Add the package brown298/reportbuilderbundle to your composer.json
```json
{
    "require" : {
        "brown298/reportbuilderbundle": "dev-master"
    }
}
```

For more information about Composer, please visit http://getcomposer.org

Add the controllers to the router

```yml
#app/config/routing.yml
brown298_report_builder:
    resource: "@Brown298ReportBuilderBundle/Resources/config/routing.yml"
    prefix:   /
```

## Create the doctrine tables:
```
app/console doctrine:schema:update --force
```
## Configure

Add Brown298ReportBuilderBundle to your application kernel

```php
// app/AppKernel.php
public function registerBundles()
{
    return array(
        // ...
        new Brown298\ReportBuilderBundle\Brown298ReportBuilderBundle(),
        // ...
    );
}
```

## Update Assetic Config
Add the report builder bundle to the assetic bundles

```yml
# Assetic Configuration
        assetic:
        bundles:        [ "Brown298ReportBuilderBundle" ]
```

##Configure Options
```yml
# config.yml
brown298_report_builder:
    builder:
        system_reports: true      # enable/disable system reports - default: true
        shared_reports: true      # enable/disable report sharing - default: true
        metadata_type: annotation # annotation/yml; where the report options are stored - default: annotation
        security_type: null       # null/role/custom; determines the security structure - default: null
        base_entities:            # starting point for the report
            - 'Full Entity Name'
```