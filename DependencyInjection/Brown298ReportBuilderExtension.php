<?php
namespace Brown298\ReportBuilderBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * Class Brown298ReportBuilderExtension
 *
 * @package Brown298\ReportBuilderBundle\DependencyInjection
 */
class Brown298ReportBuilderExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration;
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('brown298.report_builder.options', $config);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('parameters.xml');
        $loader->load('entityManager.xml');
        $loader->load('build.xml');
        $loader->load('metadata.xml');
        $loader->load('report.xml');
        $loader->load('security.xml');
        $loader->load('table.xml');

        // add aliases for metadata
        switch ($config['builder']['metadata_type']) {
            case 'annotation':
                $container->setAlias('brown298.report_builder.config_reader', 'brown298.report_builder.annotation_reader');
                break;
            case 'yml':
                $container->setAlias('brown298.report_builder.config_reader', 'brown298.report_builder.yml_reader');
                $container->setParameter('brown298.report_builder.property_metadata.class', 'Brown298\ReportBuilderBundle\Mapping\MetaData\Yml\PropertyMetadata');
                $container->setParameter('brown298.report_builder.class_metadata.class', 'Brown298\ReportBuilderBundle\Mapping\MetaData\Yml\ClassMetadata');
                break;
        }

        // add aliases for security
        switch ($config['builder']['security_type']) {
            case 'null':
                $container->setAlias('brown298.report_builder.security', 'brown298.report_builder.security.null');
                break;
            case 'role':
                $container->setAlias('brown298.report_builder.security', 'brown298.report_builder.security.role');
                break;
            default:
                $container->setAlias('brown298.report_builder.security', $config['builder']['security_type']);
                break;
        }

    }
}
