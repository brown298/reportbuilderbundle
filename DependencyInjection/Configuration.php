<?php
namespace Brown298\ReportBuilderBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package Brown298\ReportBuilderBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree.
     *
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder;
        $root = $treeBuilder->root('report_builder');

        $root
            ->children()
                ->arrayNode('builder')
                ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('enabled')->defaultTrue()->end()
                        ->booleanNode('shared_reports')->defaultTrue()->end()
                        ->booleanNode('system_reports')->defaultTrue()->end()
                        ->scalarNode('metadata_type')->defaultValue('annotation')->end()
                        ->scalarNode('security_type')->defaultValue('null')->end()
                        ->arrayNode('base_entities')
                            ->prototype('scalar')->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

    /**
     * getAliasName
     *
     * returns name for configurations
     *
     * @return string
     */
    public function getAliasName()
    {
        return 'report_builder';
    }
}